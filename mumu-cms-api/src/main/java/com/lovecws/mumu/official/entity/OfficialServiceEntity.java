package com.lovecws.mumu.official.entity;

/**
 * Created by Administrator on 2017/3/13.
 */

import com.lovecws.mumu.common.core.entity.BaseEntity;

/**
 * 官网服务实体对象
 */
public class OfficialServiceEntity extends BaseEntity{

    private Integer categoryId;//服务分类id
    private String categoryName;//服务分类名稱
    private String title;//服务标题
    private String logo;//服务logo图片
    private String content;//服务详细内容

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
