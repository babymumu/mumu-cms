package com.lovecws.mumu.official.entity;

/**
 * Created by Administrator on 2017/3/13.
 */

import com.lovecws.mumu.common.core.entity.BaseEntity;

/**
 * 官网意见反馈实体对象
 */
public class OfficialFeedbackEntity extends BaseEntity{
    private String userName;//反馈用户名称
    private String userEmail;//反馈用户邮箱地址
    private String userPhone;//用户手机号码
    private String title;//反馈标题
    private String content;//反馈内容

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
