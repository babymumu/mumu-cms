package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialServiceCategoryEntity;

import java.util.List;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialServiceCategoryService {
    /**
     * 分页获取服务列表
     * @param categoryName
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageBean<OfficialServiceCategoryEntity> listPage(String categoryName, int pageNum, int pageSize);

    /**
     * 添加服务分类
     * @param serviceCategory 服务分类实体
     */
    public void addServiceCategory(OfficialServiceCategoryEntity serviceCategory);

    /**
     * 获取服务分类详情
     * @param categoryId 服务分类id
     * @return
     */
    public OfficialServiceCategoryEntity getServiceCategoryById(int categoryId);

    /**
     * 更新服务分类
     * @param serviceCategory 服务分类实体
     */
    public void updateServiceCategory(OfficialServiceCategoryEntity serviceCategory);

    /**
     * 删除服务分类
     * @param categoryId 服务分类id
     */
    public void deleteServiceCategoryById(int categoryId);

    /**
     * 查询服务分类
     * @param categoryName
     * @return
     */
    public List<OfficialServiceCategoryEntity> getServiceCategoryByCondition(String categoryName);

}
