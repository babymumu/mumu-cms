package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialProductEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialProductService {

    /**
     * 分页获取产品列表
     * @param searchContent
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageBean<OfficialProductEntity> listPage(String searchContent, int pageNum, int pageSize);

    /**
     * 添加产品
     * @param product 产品分类id
     */
    public void addProduct(OfficialProductEntity product);

    /**
     * 根据产品id获取产品详情
     * @param productId 产品分类id
     * @return
     */
    public OfficialProductEntity getProductById(int productId);

    /**
     * 更新产品信息
     * @param product 产品分类id
     */
    public void updateProduct(OfficialProductEntity product);

    /**
     * 删除产品
     * @param productId 产品分类id
     */
    public void deleteProductById(int productId);

    /**
     * 删除该分类下的产品
     * @param categoryId 产品分类id
     */
    public void deleteProductByCategoryId(int categoryId);
}
