package com.lovecws.mumu.official.entity;

import com.lovecws.mumu.common.core.entity.BaseEntity;

/**
 * Created by ganliang on 2017/3/13.
 */

/**
 * 官网服务分类实体对象
 */
public class OfficialServiceCategoryEntity extends BaseEntity{
    private String categoryName;//分类名称

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
