package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialFeedbackEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialFeedbackService {

    /**
     * 分页获取用户反馈
     * @param searchContent
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageBean<OfficialFeedbackEntity> listPage(String searchContent, int pageNum, int pageSize);

    /**
     * 获取用户反馈详情
     * @param feedbackId
     * @return
     */
    public OfficialFeedbackEntity getFeedbackIdById(int feedbackId);

    /**
     * 删除用户反馈
     * @param feedbackId
     */
    public void deleteFeedbackById(int feedbackId);

    /**
     * 添加用户反馈
     * @param feedback
     */
    public void addFeedback(OfficialFeedbackEntity feedback);
}
