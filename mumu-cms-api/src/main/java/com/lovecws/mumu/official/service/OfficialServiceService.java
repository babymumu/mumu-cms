package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialServiceEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialServiceService {

    /**
     * 分页获取服务集合
     * @param searchContent 搜索内容
     * @param pageNum 一页数量
     * @param pageSize 一页大小
     * @return
     */
    public PageBean<OfficialServiceEntity> listPage(String searchContent, int pageNum, int pageSize);

    /**
     * 添加服务
     * @param service 服务实体
     */
    public void addService(OfficialServiceEntity service);

    /**
     * 获取服务详情
     * @param serviceId 服务id
     * @return
     */
    public OfficialServiceEntity getServiceById(int serviceId);

    /**
     * 更新服务
     * @param service 服务实体
     */
    public void updateService(OfficialServiceEntity service);

    /**
     * 删除服务
     * @param serviceId 服务id
     */
    public void deleteServiceById(int serviceId);

    /**
     * 删除服务分类下的服务
     * @param categoryId 服务分类id
     */
    public void deleteServiceByCategoryId(int categoryId);
}
