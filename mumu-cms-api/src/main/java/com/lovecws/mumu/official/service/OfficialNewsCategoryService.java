package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialNewsCategoryEntity;

import java.util.List;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialNewsCategoryService {

    /**
     * 分页获取新闻分类
     * @param categoryName
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageBean<OfficialNewsCategoryEntity> listPage(String categoryName, int pageNum, int pageSize);

    /**
     * 添加新闻分类
     * @param newsCategory 新闻分类
     */
    public void addNewsCategory(OfficialNewsCategoryEntity newsCategory);

    /**
     * 获取新闻分类详情
     * @param categoryId 新闻分类id
     * @return
     */
    public OfficialNewsCategoryEntity getNewsCategoryById(int categoryId);

    /**
     * 更新新闻分类
     * @param newsCategory 新闻分类
     */
    public void updateNewsCategory(OfficialNewsCategoryEntity newsCategory);

    /**
     * 删除新闻分类
     * @param categoryId 新闻分类id
     */
    public void deleteNewsCategoryById(int categoryId);

    /**
     * 获取新闻分类列表
     * @param categoryName
     * @return
     */
    public List<OfficialNewsCategoryEntity> getNewsCategoryByCondition(String categoryName);
}
