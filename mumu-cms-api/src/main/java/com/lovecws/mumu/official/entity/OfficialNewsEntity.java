package com.lovecws.mumu.official.entity;

/**
 * Created by Administrator on 2017/3/13.
 */

import com.lovecws.mumu.common.core.entity.BaseEntity;

/**
 * 官网留言实体对象
 */
public class OfficialNewsEntity extends BaseEntity{
    private Integer categoryId;//新闻分类id
    private String categoryName;//服务分类名稱
    private String title;//新闻标题
    private String logo;//新闻logo图片
    private String content;//新闻内容

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
