package com.lovecws.mumu.official.entity;

import com.lovecws.mumu.common.core.entity.BaseEntity;

/**
 * Created by ganliang on 2017/3/13.
 */

/**
 * 产品分类实体
 */
public class OfficialProductCategoryEntity extends BaseEntity{
    private String categoryName;//分类名称

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
