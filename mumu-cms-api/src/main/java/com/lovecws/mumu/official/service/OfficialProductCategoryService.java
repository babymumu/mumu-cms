package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialProductCategoryEntity;

import java.util.List;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialProductCategoryService {

    /**
     *分页获取产品分类
     * @param categoryName 分类名称
     * @param pageNum 当前页数
     * @param pageSize 一页大小
     * @return
     */
    public PageBean<OfficialProductCategoryEntity> listPage(String categoryName, int pageNum, int pageSize);

    /**
     * 添加产品分类
     * @param productCategoryEntity 产品分类实体
     */
    public void addProductCategory(OfficialProductCategoryEntity productCategoryEntity);

    /**
     * 获取产品分类详情
     * @param categoryId 产品分类id
     * @return
     */
    public  OfficialProductCategoryEntity getProductCategoryById(int categoryId);

    /**
     * 保存产品分类
     * @param productCategory 产品分类实体对象
     */
    public void updateProductCategory(OfficialProductCategoryEntity productCategory);

    /**
     * 删除产品分类
     * @param categoryId 产品分类id
     */
    public void deleteProductCategoryById(int categoryId);

    /**
     * 查询产品分类
     * @param categoryName 分类名称
     * @return
     */
    public List<OfficialProductCategoryEntity> getProductCategoryByCondition(String categoryName);

}
