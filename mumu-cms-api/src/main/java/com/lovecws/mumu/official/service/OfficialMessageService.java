package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialMessageEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialMessageService {
    /**
     * 分页获取留言
     * @param searchContent
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageBean<OfficialMessageEntity> listPage(String searchContent, int pageNum, int pageSize);

    /**
     * 获取留言详情
     * @param messageId 留言id
     * @return
     */
    public OfficialMessageEntity getMessageIdById(int messageId);

    /**
     * 删除留言
     * @param messageId 留言id
     */
    public void deleteMessageById(int messageId);

    /**
     * 添加留言
     * @param message 留言实体
     */
    public void addMessage(OfficialMessageEntity message);
}
