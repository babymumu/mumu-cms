package com.lovecws.mumu.official.entity;

/**
 * Created by Administrator on 2017/3/13.
 */

import com.lovecws.mumu.common.core.entity.BaseEntity;

/**
 * 官网产品实体对象
 */
public class OfficialProductEntity extends BaseEntity{

    private Integer categoryId;//产品分类id
    private String categoryName;//产品分类名称
    private String title;//产品标题
    private String logo;//产品logo图片
    private String content;//产品详细内容

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
