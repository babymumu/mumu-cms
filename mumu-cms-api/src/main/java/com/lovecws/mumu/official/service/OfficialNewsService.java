package com.lovecws.mumu.official.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialNewsEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialNewsService {

    /**
     * 分页获取新闻列表
     * @param searchContent
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageBean<OfficialNewsEntity> listPage(String searchContent, int pageNum, int pageSize);

    /**
     * 添加新闻
     * @param news 新闻
     */
    public void addNews(OfficialNewsEntity news);

    /**
     * 获取新闻详情
     * @param newsId 新闻id
     * @return
     */
    public OfficialNewsEntity getNewsById(int newsId);

    /**
     * 更新新闻
     * @param news 新闻
     */
    public void updateNews(OfficialNewsEntity news);

    /**
     * 删除新闻
     * @param newsId 新闻id
     */
    public void deleteNewsById(int newsId);

    /**
     * 删除新闻分类下的所有新闻
     * @param categoryId 新闻分类id
     */
    public void deleteNewsByCategoryId(int categoryId);
}
