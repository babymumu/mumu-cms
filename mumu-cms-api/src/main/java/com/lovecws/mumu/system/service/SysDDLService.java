package com.lovecws.mumu.system.service;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.system.entity.SysDDL;

import java.util.List;

public interface SysDDLService {

	/**
	 * 分页查询数据字典
	 * @param ddlCode 数据字典内码
	 * @param ddlName 数据字典名称 
	 * @param pageNum 当前分页数量
	 * @param pageSize 一页的数量
	 * @return
	 */
	public PageBean<SysDDL> listPage(String ddlCode, String ddlName, int pageNum, int pageSize);

	/**
	 * 添加数据字典
	 * @param ddl 数据字典实体对象
	 * @return
	 */
	public SysDDL addSysDDL(SysDDL ddl);

	/**
	 * 获取数据字典列表
	 * @param ddlCode 字典内码
	 * @param ddlName 字典名称
	 * @return
	 */
	public List<SysDDL> getSysDDLByCondition(String ddlCode, String ddlName);

	/**
	 * 通过数据字典内码删除数据字典
	 * @param ddlCode 字典内码
	 */
	public void deleteSysDDLByDDLCode(String ddlCode);

	/**
	 * 保存或者更新数据字典
	 * @param ddl 数据字典实体
	 * @return
	 */
	public SysDDL saveOrUpdateSysDDL(SysDDL ddl);

	/**
	 * 根据数据字典主键删除数据字典
	 * @param ddlId 数据字典id
	 */
	public void deleteSysDDLById(String ddlId);

	/**
	 * 获取数据字典详情
	 * @param ddlId 字典id
	 * @return
	 */
	public SysDDL getSysDDLById(String ddlId);

	/**
	 * 更新数据字典
	 * @param ddlCode 字典内码
	 * @param ddlName 字典名称
	 * @param remark 描述
	 */
	public void updateSysDDLByDDLCode(String ddlCode, String ddlName, String remark);

}
