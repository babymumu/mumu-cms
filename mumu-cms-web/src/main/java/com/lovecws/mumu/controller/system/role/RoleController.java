package com.lovecws.mumu.controller.system.role;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.system.entity.SysRole;
import com.lovecws.mumu.system.service.SysRoleService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/role")
public class RoleController {

	private static final Logger log=Logger.getLogger(RoleController.class);
	@Autowired
	private SysRoleService roleService;
	
	/**
	 * 角色列表
	 * @return
	 */
	@RequiresPermissions("system:role:view")
	@RequestMapping(value={"/list"},method=RequestMethod.GET)
	public String list(){
		return "system/role/list";
	}
	
	/**
	 * 角色列表
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:role:view")
	@RequestMapping(value={"/page"})
	public Map<String,Object> page(String roleType,String roleName,int curPage,int pageSize){
		PageBean<SysRole> pageBean = roleService.listPage(roleType,roleName, curPage, pageSize);
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("success", "true");
		page.put("totalRows", pageBean.getTotalCount());
		page.put("curPage", curPage);
		page.put("data", pageBean.getRecordList());
		return page;
	}
	
	/**
	 * 添加角色
	 * @return
	 */
	@RequiresPermissions("system:role:add")
	@RequestMapping(value={"/add"},method=RequestMethod.GET)
	public String add(){
		return "system/role/add";
	}
	
	/**
	 * 保存角色
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:role:add")
	@RequestMapping(value={"/add"},method=RequestMethod.POST)
	public ResponseEntity save(SysRole role){
		List<SysRole> roles = roleService.querySysRoleByCondition(null,role.getRoleCode(), role.getRoleName(), null);
		if(roles!=null&&roles.size()>0){
			return new ResponseEntity(500, "角色内码、角色名称不可重复", null);
		}
		try {
			role.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
			roleService.addSysRole(role);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "保存角色出现异常", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 角色详情
	 * @param roleId 角色id
	 * @return
	 */
	@RequiresPermissions("system:role:view")
	@RequestMapping(value={"/view"},method=RequestMethod.GET)
	public String view(String roleId,HttpServletRequest request){
		SysRole role = roleService.getSysRoleById(roleId);
		request.setAttribute("role", role);
		return "system/role/view";
	}
	
	/**
	 * 编辑角色
	 * @param roleId 角色id
	 * @return
	 */
	@RequiresPermissions("system:role:edit")
	@RequestMapping(value={"/edit"},method=RequestMethod.GET)
	public String edit(String roleId,HttpServletRequest request){
		SysRole role = roleService.getSysRoleById(roleId);
		request.setAttribute("role", role);
		return "system/role/edit";
	}
	
	/**
	 * 更新角色
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:role:edit")
	@RequestMapping(value={"/edit"},method=RequestMethod.PUT)
	public ResponseEntity update(SysRole role){
		List<SysRole> roles = roleService.querySysRoleByCondition(null,role.getRoleCode(), role.getRoleName(), null);
		if(roles!=null&&roles.size()>0&&roles.get(0).getRoleId()!=role.getRoleId()){
			return new ResponseEntity(500, "角色内码、角色名称不可重复", null);
		}
		try {
			role.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
			roleService.updateSysRoleById(role);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "更新角色出现异常", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 删除角色
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:role:delete")
	@RequestMapping(value={"/delete"},method=RequestMethod.DELETE)
	public ResponseEntity delete(String roleId){
		try {
			roleService.deleteById(roleId);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "删除角色出现异常", null);
		}
		return new ResponseEntity();
	}
}
