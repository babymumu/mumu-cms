package com.lovecws.mumu.controller.common.upload;

import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.UploadUtil;
import com.lovecws.mumu.storage.zimg.service.ZImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/common/upload")
public class ImageUploadController {

	@Autowired
	private ZImgService zimgService;
	
	@RequestMapping(value = "/img", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity img(HttpServletRequest request) {
		String upload=null;
		ResponseEntity responseEntity = null;
		try {
			List<String> uploadImage = UploadUtil.uploadImage(request);
			for (String filePath : uploadImage) {
				upload = zimgService.upload(filePath);
			}
			responseEntity=new ResponseEntity(200, "图片上传成功", upload);
		} catch (Exception e) {
			responseEntity=new ResponseEntity(500, "图片上传失败", null);
		}
		return responseEntity;
	}

	/**
	 * layedit 富文本编译器
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/layedit", method = RequestMethod.POST)
	@ResponseBody
	public Map layedit(HttpServletRequest request) {
		String upload=null;
		ResponseEntity responseEntity = null;
		Map<String,Object> map=new HashMap<String,Object>();
		Map<String,Object> dataMap=new HashMap<String,Object>();
		try {
			List<String> uploadImage = UploadUtil.uploadImage(request);
			for (String filePath : uploadImage) {
				upload = zimgService.uploadWithURL(filePath);
			}
			map.put("code","0");
			map.put("msg","");
			dataMap.put("src",upload);
			dataMap.put("title","");
		} catch (IOException e) {
			map.put("code","500");
			map.put("msg","图片上传失败");
			dataMap.put("src","");
			dataMap.put("title","");
		}
		map.put("data",dataMap);
		return map;
	}
}
