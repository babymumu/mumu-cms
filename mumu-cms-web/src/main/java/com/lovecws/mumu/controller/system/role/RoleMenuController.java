package com.lovecws.mumu.controller.system.role;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.tree.ZTreeBean;
import com.lovecws.mumu.controller.system.layout.LayoutController;
import com.lovecws.mumu.shiro.realm.UserRealm;
import com.lovecws.mumu.system.entity.SysMenu;
import com.lovecws.mumu.system.service.SysMenuService;
import com.lovecws.mumu.system.service.SysRoleMenuService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/system/role")
public class RoleMenuController {
	
	private static final Logger log=Logger.getLogger(RoleMenuController.class);
	@Autowired
	private SysMenuService menuService;
	@Autowired
	private SysRoleMenuService roleMenuService;
	@Autowired
	private UserRealm userRealm;
	@Autowired
	private LayoutController layoutController;
	
	/**
	 * 用户分配菜单
	 * @return
	 */
	@RequiresPermissions("system:role:allowMenu")
	@RequestMapping(value={"/allowMenu"},method=RequestMethod.GET)
	public String allowRole(String roleId,HttpServletRequest request){
		request.setAttribute("roleId", roleId);
		return "system/role/allowMenu";
	}
	
	/**
	 * 用户菜单列表
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:role:allowMenu")
	@RequestMapping(value={"/roleMenu/{roleId}"},method=RequestMethod.GET)
	public List<ZTreeBean> roleMenu(@PathVariable String roleId){
		//获取所有的菜单
		List<SysMenu> allMenus = menuService.querySysMenuByCondition(null, null, null);
		//获取用户已选择的菜单
		List<SysMenu> selectedMenus = menuService.getSysMenuByRoleId(roleId, PublicEnum.NORMAL.value());
		//Ztree 树
		List<ZTreeBean> beans=new ArrayList<ZTreeBean>();
		for (SysMenu sysMenu : allMenus) {
			boolean checked=false;
			for (SysMenu selectedMenu : selectedMenus) {
				if(sysMenu.getMenuId()==selectedMenu.getMenuId()){
					checked=true;
					break;
				}
			}
			if(sysMenu.getParentMenuId()==0){
				beans.add(new ZTreeBean(sysMenu.getMenuId().toString(), "topMenu", sysMenu.getMenuName(), true, "", checked));
			}else{
				beans.add(new ZTreeBean(sysMenu.getMenuId().toString(), sysMenu.getParentMenuId().toString(), sysMenu.getMenuName(), true, "", checked));
			}
		}
		beans.add(new ZTreeBean("topMenu", "0", "菜单树", true, "", "", false));
		//ZTreeBean bean=new ZTreeBean("topMenu", "0", "菜单树", true, "", "", false, beans);
		return beans;
	}
	
	/**
	 * 保存角色菜单
	 * @param roleId 角色id
	 * @param menuIds 菜单id（多条以逗号隔开）
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:role:allowMenu")
	@RequestMapping(value={"/roleMenu"},method=RequestMethod.POST)
	public ResponseEntity saveRolePermission(String roleId,String menuIds){
		try {
			roleMenuService.saveRoleMenu(roleId,menuIds,SecurityUtils.getSubject().getPrincipal().toString());
			//清除权限缓存
			userRealm.clearPermissionCache(null);
			//清除菜单缓存
			layoutController.clearMenuTreeCache();
		} catch (Exception e) {
			log.error(e);
			new ResponseEntity(500, "保存角色菜单出现异常", null);
		}
		return new ResponseEntity(200,"角色分配菜单操作成功",null);
	}
}
