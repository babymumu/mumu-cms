package com.lovecws.mumu.controller.portal.account;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/portal/account")
public class PasswordController {

	//@Autowired
	//private SimpleEmailService emailService;
	/**
	 * 跳转到找回密码列表页面
	 * @return
	 */
	@RequestMapping(value={"/pwd"},method=RequestMethod.GET)
	public String pwd(){
		return "portal/account/pwd";
	}
	
	
	/**
	 * 跳转邮箱找回密码页面
	 * @return
	 */
	@RequestMapping(value={"/pwdEmail"},method=RequestMethod.GET)
	public String pwdEmail(){
		return "portal/account/pwdEmail";
	}
	
	/**
	 * 邮件发送
	 * @param email 邮箱号码
	 * @return
	 */
	@RequestMapping(value={"/pwdEmailSuccess"},method=RequestMethod.POST)
	public String pwdEmailSuccess(String email){
		return "portal/account/pwdEmailSuccess";
	}
	
	/**
	 * 跳转手机号码找回密码页面
	 * @return
	 */
	@RequestMapping(value={"/pwdPhone"},method=RequestMethod.GET)
	public String pwdPhone(){
		return "portal/account/pwdPhone";
	}

	/**
	 * 进入人工服务找回密码页面
	 * @return
	 */
	@RequestMapping(value={"/pwdService"},method=RequestMethod.GET)
	public String pwdService(){return "portal/account/pwdService";}

	/**
	 * 跳转修改密码详情页面
	 * @param userPhone 用户手机号码
	 * @param userEmail 用户邮箱
	 * @return
	 */
	@RequestMapping(value={"/pwdDetail"},method={RequestMethod.GET,RequestMethod.POST})
	public String pwdDetail(String userPhone,String userEmail,HttpServletRequest request){
		request.setAttribute("userPhone",userPhone);
		request.setAttribute("userEmail",userEmail);
		return "portal/account/pwdDetail";
	}
	
	/**
	 * 跳转密码找回成功页面
	 * @return
	 */
	@RequestMapping(value={"/pwdSuccess"},method=RequestMethod.POST)
	public String pwdSuccess(){
		return "portal/account/pwdSuccess";
	}
}
