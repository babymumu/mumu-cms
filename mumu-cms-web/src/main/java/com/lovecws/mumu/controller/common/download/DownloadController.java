package com.lovecws.mumu.controller.common.download;

import com.lovecws.mumu.common.core.utils.HttpClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Controller
@RequestMapping("/common/download")
public class DownloadController {

	@ResponseBody
	@RequestMapping(value="/json",method=RequestMethod.GET)
	public String json(String url){
		//String url="http://data.hcharts.cn/jsonp.php?filename=GeoMap/json/xi_zang.geo.json";
		String json = HttpClientUtil.get(url);
		int lastIndexOf = url.lastIndexOf("/");
		File file=new File("D:/mapdata/data"+url.substring(lastIndexOf));
		if(file.exists()){
			return null;
		}
		BufferedWriter writer=null;
		try {
		    writer=new BufferedWriter(new FileWriter(file));
			writer.write(json);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("下载完成"+url);
		return null;
	}
}
