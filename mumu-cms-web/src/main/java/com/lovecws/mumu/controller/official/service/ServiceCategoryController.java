package com.lovecws.mumu.controller.official.service;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.official.entity.OfficialServiceCategoryEntity;
import com.lovecws.mumu.official.service.OfficialServiceCategoryService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/13.
 */
@Controller
@RequestMapping("/official/service")
public class ServiceCategoryController {

    @Autowired
    private OfficialServiceCategoryService serviceCategoryService;

    /**
     * 服务分类
     * @return
     */
    @RequiresPermissions("official:service:categoryView")
    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String serviceCategory(){
        return "official/service/category";
    }

    /**
     *分页获取服务分类数据
     * @param categoryName 服务分类名称
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:categoryView")
    @RequestMapping(value = "/categoryPage", method = RequestMethod.GET)
    public Map<String,Object> serviceCategoryPage(String categoryName,int beginIndex,int pageSize){
        PageBean<OfficialServiceCategoryEntity> pageBean=serviceCategoryService.listPage(categoryName,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 跳转到添加服务分类页面
     * @return
     */
    @RequiresPermissions("official:service:categoryAdd")
    @RequestMapping(value = "/categoryAdd", method = RequestMethod.GET)
    public String serviceCategoryAdd(){
        return "official/service/categoryAdd";
    }

    /**
     * 添加服务分类
     * @param categoryName 服务分类名称
     * @param remark 服务分类描述
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:categoryAdd")
    @RequestMapping(value = "/categoryAdd", method = RequestMethod.POST)
    public ResponseEntity saveServiceCategory(String categoryName,String remark){
        //校验参数
        if(StringUtil.isEmpty(categoryName)){
            return new ResponseEntity(400,"服务分类不能为空!",null);
        }
        OfficialServiceCategoryEntity serviceCategory=new OfficialServiceCategoryEntity();
        serviceCategory.setCreateTime(new Date());
        serviceCategory.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        serviceCategory.setCategoryName(categoryName);
        serviceCategory.setStatus(PublicEnum.NORMAL.value());
        serviceCategory.setRemark(remark);
        serviceCategoryService.addServiceCategory(serviceCategory);
        return new ResponseEntity(200,"保存服务分类操作成功!",null);
    }

    /**
     * 查看服务分类详情
     * @param categoryId 服务分类id
     * @param request
     * @return
     */
    @RequiresPermissions("official:service:categoryView")
    @RequestMapping(value = "/categoryView/{categoryId}", method = RequestMethod.GET)
    public String serviceCategoryView(@PathVariable int categoryId, HttpServletRequest request){
        OfficialServiceCategoryEntity serviceCategoryEntity=serviceCategoryService.getServiceCategoryById(categoryId);
        request.setAttribute("serviceCategory",serviceCategoryEntity);
        return "official/service/categoryView";
    }

    /**
     * 跳转到编辑服务分类页面
     * @param categoryId 服务分类id
     * @param request
     * @return
     */
    @RequiresPermissions("official:service:categoryEdit")
    @RequestMapping(value = "/categoryEdit/{categoryId}", method = RequestMethod.GET)
    public String serviceCategoryEdit(@PathVariable int categoryId, HttpServletRequest request){
        OfficialServiceCategoryEntity serviceCategoryEntity=serviceCategoryService.getServiceCategoryById(categoryId);
        request.setAttribute("serviceCategory",serviceCategoryEntity);
        return "official/service/categoryEdit";
    }

    /**
     * 更新服务分类
     * @param categoryId 服务分类id
     * @param categoryName 服务分类名称
     * @param remark 服务分类描述
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:categoryEdit")
    @RequestMapping(value = "/categoryEdit", method = RequestMethod.POST)
    public ResponseEntity updateServiceCategory(int categoryId,String categoryName,String remark){
        OfficialServiceCategoryEntity serviceCategoryEntity=serviceCategoryService.getServiceCategoryById(categoryId);
        if(serviceCategoryEntity==null){
            return new ResponseEntity(400,"服务分类不存在",null);
        }
        OfficialServiceCategoryEntity serviceCategory=new OfficialServiceCategoryEntity();
        serviceCategory.setId(serviceCategoryEntity.getId());
        serviceCategory.setCategoryName(categoryName);
        serviceCategory.setRemark(remark);
        serviceCategory.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
        serviceCategory.setEditTime(new Date());
        serviceCategoryService.updateServiceCategory(serviceCategory);
        return new ResponseEntity(200,"保存服务分类操作成功!",null);
    }

    /**
     * 删除服务分类
     * @param categoryId 服务分类id
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:categoryDelete")
    @RequestMapping(value = "/categoryDelete/{categoryId}", method = RequestMethod.DELETE)
    public ResponseEntity updateServiceCategory(@PathVariable int categoryId){
        OfficialServiceCategoryEntity serviceCategoryEntity=serviceCategoryService.getServiceCategoryById(categoryId);
        if(serviceCategoryEntity==null){
            return new ResponseEntity(400,"服务分类不存在",null);
        }
        serviceCategoryService.deleteServiceCategoryById(categoryId);
        return new ResponseEntity(200,"删除服务分类操作成功",null);
    }
}
