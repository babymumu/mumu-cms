package com.lovecws.mumu.controller.official.feedback;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.official.entity.OfficialFeedbackEntity;
import com.lovecws.mumu.official.service.OfficialFeedbackService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ganliang on 2017/3/13.
 * 用户反馈集合控制器
 */
@Controller
@RequestMapping("/official/feedback")
public class FeedbackController {

    @Autowired
    private OfficialFeedbackService feedbackService;

    @RequiresPermissions("official:feedback:view")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(){
        return "official/feedback/list";
    }

    /**
     *分页获取用户反馈列表
     * @param searchContent 搜索内容(标题或者分类id)
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:feedback:view")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Map<String,Object> page(String searchContent, int beginIndex, int pageSize){
        PageBean<OfficialFeedbackEntity> pageBean=feedbackService.listPage(searchContent,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 查看用户反馈分类详情
     * @param feedbackId 用户反馈id
     * @param request
     * @return
     */
    @RequiresPermissions("official:feedback:view")
    @RequestMapping(value = "/view/{feedbackId}", method = RequestMethod.GET)
    public String productsView(@PathVariable int feedbackId, HttpServletRequest request){
        OfficialFeedbackEntity feedback=feedbackService.getFeedbackIdById(feedbackId);
        request.setAttribute("feedback",feedback);
        return "official/feedback/view";
    }

    /**
     * 删除用户反馈
     * @param feedbackId 用户反馈id
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:feedback:delete")
    @RequestMapping(value = "/delete/{feedbackId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable int feedbackId){
        OfficialFeedbackEntity feedback=feedbackService.getFeedbackIdById(feedbackId);
        if(feedback==null){
            return new ResponseEntity(400,"用户反馈不存在",null);
        }
        feedbackService.deleteFeedbackById(feedbackId);
        return new ResponseEntity(200,"删除用户反馈操作成功",null);
    }

    /**
     * 添加用户反馈
     * @param userName 用户名称
     * @param userEmail 用户邮箱
     * @param userPhone 用户手机号码
     * @param title 标题
     * @param content 内容
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity add(String userName,String userEmail,String userPhone,String title,String content){
        OfficialFeedbackEntity feedback=new OfficialFeedbackEntity();
        feedback.setStatus(PublicEnum.NORMAL.value());
        feedback.setCreateTime(new Date());
        feedback.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        feedback.setUserName(userName);
        feedback.setUserPhone(userPhone);
        feedback.setUserEmail(userEmail);
        feedback.setTitle(title);
        feedback.setContent(content);
        feedbackService.addFeedback(feedback);
        return new ResponseEntity(200,"添加用户反馈操作成功",null);
    }
}
