 package com.lovecws.mumu.controller.system.menu;

 import com.lovecws.mumu.common.core.enums.PublicEnum;
 import com.lovecws.mumu.common.core.response.ResponseEntity;
 import com.lovecws.mumu.system.entity.SysPermission;
 import com.lovecws.mumu.system.service.SysPermissionService;
 import org.apache.log4j.Logger;
 import org.apache.shiro.SecurityUtils;
 import org.apache.shiro.authz.annotation.RequiresPermissions;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.beans.propertyeditors.CustomDateEditor;
 import org.springframework.stereotype.Controller;
 import org.springframework.web.bind.WebDataBinder;
 import org.springframework.web.bind.annotation.InitBinder;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestMethod;
 import org.springframework.web.bind.annotation.ResponseBody;

 import javax.servlet.http.HttpServletRequest;
 import java.text.DateFormat;
 import java.text.SimpleDateFormat;
 import java.util.Date;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;

@Controller
@RequestMapping("/system/menu")
public class MenuPermissionController {

	@Autowired
	private SysPermissionService permissionService;
	
	private static final Logger log=Logger.getLogger(MenuPermissionController.class);
	/**
	 * 进入菜单权限列表
	 * @param menuId 菜单id
	 * @return
	 */
	@RequiresPermissions("system:menu:permission")
	@RequestMapping(value="/menuPermission",method=RequestMethod.GET)
	public String menuPermission(String menuId,HttpServletRequest request){
		request.setAttribute("menuId", menuId);
		return "system/menu/menuPermission";
	}
	
	/**
	 * 获取菜单下的权限列表
	 * @param menuId 菜单id
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:permission")
	@RequestMapping(value="/menuPermissionData",method=RequestMethod.GET)
	public Map<String, Object> menuPermissionData(String menuId){
		List<SysPermission> permissions= permissionService.querySysPermissionByCondition(menuId, null, null, PublicEnum.NORMAL.value());
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("total", permissions.size());
		page.put("rows", permissions);
		return page;
	}
	
	/**
	 * 更新或者保存权限
	 * @param permission 权限实体
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:permission")
	@RequestMapping(value="/deleteMenuPermission",method=RequestMethod.DELETE)
	public ResponseEntity deleteMenuPermission(String permissionId){
		try {
			permissionService.deletePermissionById(permissionId);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "删除菜单权限出现异常", null);
		}
		return new ResponseEntity();
	}

	/**
	 * 更新或者保存权限
	 * @param permission 权限实体
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:permission")
	@RequestMapping(value="/saveMenuPermission",method=RequestMethod.POST)
	public ResponseEntity saveMenuPermission(SysPermission permission){
		//获取权限内码和权限名称下面的权限列表
		List<SysPermission> permissions = permissionService.querySysPermissionByCondition(null, permission.getPermissionCode(), permission.getPermissionName(), PublicEnum.NORMAL.value());
		if(permissions!=null&&permissions.size()>1){
			return new ResponseEntity(500, "权限内码、权限名称不可重复", null);
		}
		if(permissions!=null&&permissions.size()==1){
			if(permission.getPermissionId()!=null&&permissions.get(0).getPermissionId()!=permission.getPermissionId()){
				return new ResponseEntity(500, "权限内码、权限名称不可重复", null);
			}
		}
		try {
			if(permission.getPermissionId()==0){
				permission.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
				permissionService.addPermission(permission);
			}else{
				permission.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
				permissionService.updatePermissionById(permission);
			}
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "保存或者更新权限出现异常", null);
		}
		return new ResponseEntity();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		dateFormat.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
