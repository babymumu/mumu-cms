package com.lovecws.mumu.controller.system.log;

import com.lovecws.mumu.system.entity.SysUserLog;
import com.lovecws.mumu.system.service.SysUserLogService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;


/**
 * 监听队列
 * @author ShenHuaJie
 * @version 2016年6月14日 上午11:00:53
 */
public class LogMessageListener implements MessageListener {
	private final Logger logger = Logger.getLogger(LogMessageListener.class);

	@Autowired
	private SysUserLogService logService;
	@Override
	public void onMessage(Message message) {
		try {
			Serializable object = ((ObjectMessage) message).getObject();
			//添加日志
			logService.addSysUserLog((SysUserLog)object);
		} catch (JMSException e) {
			logger.error(e);
		}
	}
}
