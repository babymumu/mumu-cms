package com.lovecws.mumu.controller.system.layout;

import com.lovecws.mumu.cache.redis.JedisClient;
import com.lovecws.mumu.common.core.response.HttpCode;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.tree.MenuTree;
import com.lovecws.mumu.common.core.utils.SerializeUtil;
import com.lovecws.mumu.shiro.entity.BaseRealm;
import com.lovecws.mumu.shiro.utils.PasswordHelper;
import com.lovecws.mumu.system.entity.SysMenu;
import com.lovecws.mumu.system.entity.SysUser;
import com.lovecws.mumu.system.service.SysMenuService;
import com.lovecws.mumu.system.service.SysUserService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/system/layout")
public class LayoutController {
	
	@Autowired
	private SysMenuService menuService;
	@Autowired
	private SysUserService userService;
	@Autowired
	private JedisClient jedisClient;
	
	/**
	 * 左侧菜单功能栏
	 * @return
	 */
	@RequestMapping(value={"/left"},method=RequestMethod.GET)
	public String left(HttpServletRequest request){
		String loginName = SecurityUtils.getSubject().getPrincipal().toString();
		byte[] cacheNameBytes=("mumu:cms:menuTree:"+loginName).getBytes();
		//獲取菜单树緩存
		byte[] menuTreeBytes=jedisClient.get(cacheNameBytes);
		if(menuTreeBytes==null){
			//获取保存在session中的用户
			SysUser user=(SysUser) SecurityUtils.getSubject().getSession().getAttribute(SysUser.SYS_USER);
			//获取登录用户的菜单
			List<SysMenu> menus=menuService.getSysMenuByUserId(user.getUserId());
			//组建菜单树
			List<MenuTree> tree=new ArrayList<MenuTree>();
			for (SysMenu sysMenu : menus) {
				tree.add(new MenuTree(sysMenu.getMenuId(), sysMenu.getParentMenuId(), sysMenu.getMenuIcon(), sysMenu.getMenuUrl(), sysMenu.getMenuName()));
			}
			//树形化
			List<MenuTree> menuTree = MenuTree.tree(tree);
			//加入缓存 缓存30分钟
			menuTreeBytes=SerializeUtil.toBytes(menuTree);
			jedisClient.set(cacheNameBytes,menuTreeBytes,30*60);
		}
		request.setAttribute("menuTree",SerializeUtil.toObject(menuTreeBytes));
		return "system/layout/left";
	}

	/**
	 * 删除当前登录用户的菜单树
	 */
	public void clearMenuTreeCache(){
		String loginName = SecurityUtils.getSubject().getPrincipal().toString();
		jedisClient.del("mumu:cms:menuTree:"+loginName);
	}
	
	/**
	 * 主要功能界面
	 * @return
	 */
	@RequestMapping(value={"/main"},method=RequestMethod.GET)
	public String main(){
		return "system/layout/main";
	}
	
	/**
	 * 首页
	 * @return
	 */
	@RequestMapping(value={"/index"},method=RequestMethod.GET)
	public String index(){
		return "system/layout/index";
	}

	/**
	 * 登录用户详情
	 * @return
	 */
	@RequestMapping(value={"/user"},method=RequestMethod.GET)
	public String user(HttpServletRequest request){
		SysUser sysUser=(SysUser)SecurityUtils.getSubject().getSession().getAttribute(SysUser.SYS_USER);
		request.setAttribute("user",sysUser);
		//return "system/layout/user";
		return "system/user/view";
	}

	/**
	 * 跳转到修改用户密码页面
	 * @return
	 */
	@RequestMapping(value={"/password"},method=RequestMethod.GET)
	public String password(){
		return "system/layout/password";
	}


	/**
	 * 修改用户密码
	 * @param password 用户原密码
	 * @param newPassword 用户新密码
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value={"/password"},method=RequestMethod.POST)
	public ResponseEntity changePassword(String password,String newPassword){
		//从session中获取当前用户信息
		SysUser sysUser=(SysUser)SecurityUtils.getSubject().getSession().getAttribute(SysUser.SYS_USER);
		if(sysUser==null){
			return new ResponseEntity(HttpCode.SERVER_ERROR,"服务器出现未知异常!");
		}
		sysUser=userService.getSysUserById(String.valueOf(sysUser.getUserId()));
		//比对密码
		if(!sysUser.getPassword().equals(PasswordHelper.getPwd(password,sysUser.getUserName(),sysUser.getSalt()))){
			return new ResponseEntity(HttpCode.PARAMETER_ERROR,"用户密码不正确!");
		}
		//加密新密码
		BaseRealm baseRealm = PasswordHelper.encryptPassword(new BaseRealm(sysUser.getUserName(), newPassword));
		SysUser user=new SysUser();
		user.setUserId(sysUser.getUserId());
		user.setUserName(sysUser.getUserName());
		user.setPassword(baseRealm.getPassword());
		user.setSalt(baseRealm.getSalt());
		user.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
		userService.updateById(user);
		return new ResponseEntity();
	}
}
