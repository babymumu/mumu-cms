package com.lovecws.mumu.controller.official.product;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.official.entity.OfficialProductCategoryEntity;
import com.lovecws.mumu.official.entity.OfficialProductEntity;
import com.lovecws.mumu.official.service.OfficialProductCategoryService;
import com.lovecws.mumu.official.service.OfficialProductService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ganliang on 2017/3/13.
 * 产品集合控制器
 */
@Controller
@RequestMapping("/official/product")
public class ProductController {

    @Autowired
    private OfficialProductService productService;
    @Autowired
    private OfficialProductCategoryService productCategoryService;

    @RequiresPermissions(value = {"official:product:view","official:product:categoryView"},logical= Logical.OR)
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(){
        return "official/product/list";
    }

    /**
     * 产品列表
     * @return
     */
    @RequiresPermissions(value = {"official:product:view"})
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String products(){
        return "official/product/products";
    }

    /**
     *分页获取产品列表
     * @param searchContent 搜索内容(标题或者分类id)
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:view"})
    @RequestMapping(value = "/productsPage", method = RequestMethod.GET)
    public Map<String,Object> productPage(String searchContent, int beginIndex, int pageSize){
        PageBean<OfficialProductEntity> pageBean=productService.listPage(searchContent,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 跳转到添加产品列表页面
     * @return
     */
    @RequiresPermissions(value = {"official:product:add"})
    @RequestMapping(value = "/productsAdd", method = RequestMethod.GET)
    public String productsAdd(HttpServletRequest request){
        List<OfficialProductCategoryEntity> productCategorys=productCategoryService.getProductCategoryByCondition(null);
        request.setAttribute("productCategorys",productCategorys);
        return "official/product/productsAdd";
    }

    /**
     * 添加产品
     * @param title 产品标题
     * @param categoryId 产品分类
     * @param logo 产品logo
     * @param content 产品内容
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:add"})
    @RequestMapping(value = "/productsAdd", method = RequestMethod.POST)
    public ResponseEntity saveProducts(String title,int categoryId,String logo,String content){
        //校验参数
        if(StringUtil.isEmpty(title)){
            return new ResponseEntity(400,"产品标题不能为空!",null);
        }
        if(StringUtil.isEmpty(logo)){
            return new ResponseEntity(400,"产品logo图标不能为空!",null);
        }
        OfficialProductEntity product=new OfficialProductEntity();
        product.setCreateTime(new Date());
        product.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        product.setStatus(PublicEnum.NORMAL.value());
        product.setTitle(title);
        product.setCategoryId(categoryId);
        product.setLogo(logo);
        product.setContent(content);
        productService.addProduct(product);
        return new ResponseEntity(200,"保存产品操作成功!",null);
    }

    /**
     * 查看产品分类详情
     * @param productId 产品id
     * @param request
     * @return
     */
    @RequiresPermissions(value = {"official:product:view"})
    @RequestMapping(value = "/productsView/{productId}", method = RequestMethod.GET)
    public String productsView(@PathVariable int productId, HttpServletRequest request){
        OfficialProductEntity product=productService.getProductById(productId);
        request.setAttribute("product",product);
        return "official/product/productsView";
    }

    /**
     * 跳转到编辑产品页面
     * @param productId 产品id
     * @param request
     * @return
     */
    @RequiresPermissions(value = {"official:product:edit"})
    @RequestMapping(value = "/productsEdit/{productId}", method = RequestMethod.GET)
    public String productsEdit(@PathVariable int productId, HttpServletRequest request){
        OfficialProductEntity product=productService.getProductById(productId);
        request.setAttribute("product",product);

        List<OfficialProductCategoryEntity> productCategorys=productCategoryService.getProductCategoryByCondition(null);
        request.setAttribute("productCategorys",productCategorys);
        return "official/product/productsEdit";
    }

    /**
     * 更新产品
     * @param productId
     * @param title
     * @param categoryId
     * @param logo
     * @param content
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:edit"})
    @RequestMapping(value = "/productsEdit", method = RequestMethod.POST)
    public ResponseEntity productsEdit(int productId,String title,int categoryId,String logo,String content){
        OfficialProductEntity productEntity=productService.getProductById(productId);
        if(productEntity==null){
            return new ResponseEntity(400,"产品不存在",null);
        }
        OfficialProductEntity product=new OfficialProductEntity();
        product.setId(productId);
        product.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
        product.setEditTime(new Date());
        product.setTitle(title);
        product.setCategoryId(categoryId);
        product.setLogo(logo);
        product.setContent(content);
        productService.updateProduct(product);
        return new ResponseEntity(200,"保存产品操作成功!",null);
    }

    /**
     * 删除产品
     * @param productId 产品id
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:delete"})
    @RequestMapping(value = "/productsDelete/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity productsDelete(@PathVariable int productId){
        OfficialProductEntity product=productService.getProductById(productId);
        if(product==null){
            return new ResponseEntity(400,"产品不存在",null);
        }
        productService.deleteProductById(productId);
        return new ResponseEntity(200,"删除产品操作成功",null);
    }

    /**
     * 获取只是显示的产品列表
     * @param categoryId 产品分类id
     * @param request
     * @return
     */
    @RequiresPermissions(value = {"official:product:view"})
    @RequestMapping(value = "/productsList/{categoryId}", method = RequestMethod.GET)
    public String productsList(@PathVariable int categoryId,HttpServletRequest request){
        request.setAttribute("cid",categoryId);
        return "official/product/productsList";
    }
}
