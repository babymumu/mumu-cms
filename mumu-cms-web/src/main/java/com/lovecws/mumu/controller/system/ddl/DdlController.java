package com.lovecws.mumu.controller.system.ddl;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.system.entity.SysDDL;
import com.lovecws.mumu.system.service.SysDDLService;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/system/ddl")
public class DdlController {
	
	private static final Logger log=Logger.getLogger(DdlController.class);
	@Autowired
	private SysDDLService ddlService;

	/**
	 * 数据字典列表
	 *
	 * @return
	 */
	@RequiresPermissions("system:ddl:view")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list() {
		return "system/ddl/list";
	}

	/**
	 * 数据字典列表
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:view")
	@RequestMapping(value={"/page"})
	public Map<String,Object> page(String ddlCode,String ddlName,int beginIndex,int pageSize){
		//分页查询
		PageBean<SysDDL> pageBean=ddlService.listPage(ddlCode,ddlName,beginIndex/pageSize+1,pageSize);
		Map<String,Object> page=new HashMap<String,Object>();
		page.put("total", pageBean.getTotalCount());
		page.put("rows", pageBean.getRecordList());
		return page;
	}
	
	/**
	 * 添加数据字典
	 * @return
	 */
	@RequiresPermissions("system:ddl:add")
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(){
		return "system/ddl/add";
	}
	
	/**
	 * 校验数据字典是否存在
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/exists",method=RequestMethod.GET)
	public ResponseEntity exists(String ddlCode, String ddlName){
		List<SysDDL> ddls=ddlService.getSysDDLByCondition(ddlCode,ddlName);
		if(ddls!=null&&ddls.size()>0){
			return new ResponseEntity(500, "数据字典内码、数据字典名称不可重复", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 保存数据字典
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:add")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity save(String ddlCode,String ddlName,String remark){
		try {
			SysDDL ddl=new SysDDL();
			ddl.setDdlCode(ddlCode);
			ddl.setDdlName(ddlName);
			ddl.setRemark(remark);
			ddl=ddlService.addSysDDL(ddl);
			return new ResponseEntity(ddl);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "保存失败", null);
		}
	}
	
	/**
	 * 删除数据字典
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:delete")
	@RequestMapping(value="/delete",method=RequestMethod.DELETE)
	public ResponseEntity delete(String ddlCode){
		try {
			ddlService.deleteSysDDLByDDLCode(ddlCode);
			return new ResponseEntity();
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "删除失败", null);
		}
	}
	
	/**
	 * 跳转到子数据字典页面
	 * @param ddlCode 数据字典内码
	 * @return
	 */
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/subDDL",method=RequestMethod.GET)
	public String subDDL(String ddlCode,String ddlName,HttpServletRequest request){
		request.setAttribute("ddlCode", ddlCode);
		request.setAttribute("ddlName", ddlName);
		return "system/ddl/subDDL";
	}
	
	/**
	 * 数据字典子集
	 * @param ddlCode 数据字典内码
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/ddlSet",method=RequestMethod.GET)
	public Map<String,Object> DDLSet(String ddlCode){
		List<SysDDL> ddls = ddlService.getSysDDLByCondition(ddlCode, null);
		//去除空值
		List<SysDDL> sysddls=new ArrayList<SysDDL>();
		for (SysDDL sysDDL : ddls) {
			if(sysDDL.getDdlKey()!=null&&!"".equals(sysDDL.getDdlKey())){
				sysddls.add(sysDDL);
			}
		}
		Map<String,Object> page=new HashMap<String,Object>();
		page.put("total", sysddls.size());
		page.put("rows",sysddls);
		return page;
	}
	
	/**
	 * 保存数据字典集合
	 * @param ddlCode 数据字典内码
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/ddlSet",method=RequestMethod.POST)
	public ResponseEntity saveDDLSet(SysDDL ddl){
		try {
			ddl=ddlService.saveOrUpdateSysDDL(ddl);
			return new ResponseEntity(ddl);
		} catch (Exception e) {
			return new ResponseEntity(500, "保存失败", ddl);
		}
	}
	
	/**
	 * 删除数据字典
	 * @param ddlId 数据字典id
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/ddlSet",method=RequestMethod.DELETE)
	public ResponseEntity deleteDDLSet(String ddlId){
		try {
			ddlService.deleteSysDDLById(ddlId);
			return new ResponseEntity();
		} catch (Exception e) {
			return new ResponseEntity(500, "保存失败", null);
		}
	}
	
	/**
	 * 字典详情
	 * @param ddlCode 数据字典内码
	 * @return
	 */
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String view(String ddlCode,HttpServletRequest request){
		SysDDL ddl=ddlService.getSysDDLByCondition(ddlCode, null).get(0);
		request.setAttribute("ddl", ddl);
		return "system/ddl/view";
	}
	
	/**
	 * 编辑数据字典
	 * @param ddlCode 数据字典内码
	 * @return
	 */
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public String edit(String ddlCode,HttpServletRequest request){
		SysDDL ddl=ddlService.getSysDDLByCondition(ddlCode, null).get(0);
		request.setAttribute("ddl", ddl);
		return "system/ddl/edit";
	}
	
	/**
	 * 编辑数据字典
	 * @param ddlCode 数据字典内码
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:ddl:dataset")
	@RequestMapping(value="/edit",method=RequestMethod.PUT)
	public ResponseEntity updateDDL(String ddlCode,String ddlName,String remark,HttpServletRequest request){
		try {
			ddlService.updateSysDDLByDDLCode(ddlCode,ddlName,remark);
			return new ResponseEntity();
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "更新失败", null);
		}
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		dateFormat.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
