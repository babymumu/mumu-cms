package com.lovecws.mumu.controller.official.news;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.official.entity.OfficialNewsCategoryEntity;
import com.lovecws.mumu.official.service.OfficialNewsCategoryService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/13.
 */
@Controller
@RequestMapping("/official/news")
public class NewsCategoryController {

    @Autowired
    private OfficialNewsCategoryService newsCategoryService;

    /**
     * 新闻分类
     * @return
     */
    @RequiresPermissions("official:news:categoryView")
    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String newsCategory(){
        return "official/news/category";
    }

    /**
     *分页获取新闻分类数据
     * @param categoryName 新闻分类名称
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:categoryView")
    @RequestMapping(value = "/categoryPage", method = RequestMethod.GET)
    public Map<String,Object> newsCategoryPage(String categoryName,int beginIndex,int pageSize){
        PageBean<OfficialNewsCategoryEntity> pageBean=newsCategoryService.listPage(categoryName,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 跳转到添加新闻分类页面
     * @return
     */
    @RequiresPermissions("official:news:categoryAdd")
    @RequestMapping(value = "/categoryAdd", method = RequestMethod.GET)
    public String newsCategoryAdd(){
        return "official/news/categoryAdd";
    }

    /**
     * 添加新闻分类
     * @param categoryName 新闻分类名称
     * @param remark 新闻分类描述
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:categoryAdd")
    @RequestMapping(value = "/categoryAdd", method = RequestMethod.POST)
    public ResponseEntity saveNewsCategory(String categoryName,String remark){
        //校验参数
        if(StringUtil.isEmpty(categoryName)){
            return new ResponseEntity(400,"新闻分类不能为空!",null);
        }
        OfficialNewsCategoryEntity newsCategory=new OfficialNewsCategoryEntity();
        newsCategory.setCreateTime(new Date());
        newsCategory.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        newsCategory.setCategoryName(categoryName);
        newsCategory.setStatus(PublicEnum.NORMAL.value());
        newsCategory.setRemark(remark);
        newsCategoryService.addNewsCategory(newsCategory);
        return new ResponseEntity(200,"保存新闻分类操作成功!",null);
    }

    /**
     * 查看新闻分类详情
     * @param categoryId 新闻分类id
     * @param request
     * @return
     */
    @RequiresPermissions("official:news:categoryView")
    @RequestMapping(value = "/categoryView/{categoryId}", method = RequestMethod.GET)
    public String newsCategoryView(@PathVariable int categoryId, HttpServletRequest request){
        OfficialNewsCategoryEntity newsCategoryEntity=newsCategoryService.getNewsCategoryById(categoryId);
        request.setAttribute("newsCategory",newsCategoryEntity);
        return "official/news/categoryView";
    }

    /**
     * 跳转到编辑新闻分类页面
     * @param categoryId 新闻分类id
     * @param request
     * @return
     */
    @RequiresPermissions("official:news:categoryEdit")
    @RequestMapping(value = "/categoryEdit/{categoryId}", method = RequestMethod.GET)
    public String newsCategoryEdit(@PathVariable int categoryId, HttpServletRequest request){
        OfficialNewsCategoryEntity newsCategoryEntity=newsCategoryService.getNewsCategoryById(categoryId);
        request.setAttribute("newsCategory",newsCategoryEntity);
        return "official/news/categoryEdit";
    }

    /**
     * 更新新闻分类
     * @param categoryId 新闻分类id
     * @param categoryName 新闻分类名称
     * @param remark 新闻分类描述
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:categoryEdit")
    @RequestMapping(value = "/categoryEdit", method = RequestMethod.POST)
    public ResponseEntity updateNewsCategory(int categoryId,String categoryName,String remark){
        OfficialNewsCategoryEntity newsCategoryEntity=newsCategoryService.getNewsCategoryById(categoryId);
        if(newsCategoryEntity==null){
            return new ResponseEntity(400,"新闻分类不存在",null);
        }
        OfficialNewsCategoryEntity newsCategory=new OfficialNewsCategoryEntity();
        newsCategory.setId(newsCategoryEntity.getId());
        newsCategory.setCategoryName(categoryName);
        newsCategory.setRemark(remark);
        newsCategory.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
        newsCategory.setEditTime(new Date());
        newsCategoryService.updateNewsCategory(newsCategory);
        return new ResponseEntity(200,"保存新闻分类操作成功!",null);
    }

    /**
     * 删除新闻分类
     * @param categoryId 新闻分类id
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:categoryDelete")
    @RequestMapping(value = "/categoryDelete/{categoryId}", method = RequestMethod.DELETE)
    public ResponseEntity updateNewsCategory(@PathVariable int categoryId){
        OfficialNewsCategoryEntity newsCategoryEntity=newsCategoryService.getNewsCategoryById(categoryId);
        if(newsCategoryEntity==null){
            return new ResponseEntity(400,"新闻分类不存在",null);
        }
        newsCategoryService.deleteNewsCategoryById(categoryId);
        return new ResponseEntity(200,"删除新闻分类操作成功",null);
    }
}
