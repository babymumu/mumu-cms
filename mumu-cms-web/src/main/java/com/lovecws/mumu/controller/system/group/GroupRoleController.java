package com.lovecws.mumu.controller.system.group;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.tree.ZTreeBean;
import com.lovecws.mumu.system.entity.SysRole;
import com.lovecws.mumu.system.service.SysGroupRoleService;
import com.lovecws.mumu.system.service.SysRoleService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/system/group")
public class GroupRoleController {

	private static final Logger log=Logger.getLogger(GroupRoleController.class);
	@Autowired
	private SysGroupRoleService groupRoleService;
	@Autowired
	private SysRoleService roleService;
	
	/**
	 * 用户组分配角色
	 * @param groupId 用户组id
	 * @return
	 */
	@RequiresPermissions("system:group:allowRole")
	@RequestMapping(value={"/allowRole"},method=RequestMethod.GET)
	public String allowRole(String groupId,HttpServletRequest request){
		request.setAttribute("groupId", groupId);
		return "system/group/allowRole";
	}
	
	/**
	 * 用户组分配角色
	 * @param groupId 用户组id
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:group:allowRole")
	@RequestMapping(value={"/groupRole/{groupId}"},method=RequestMethod.GET)
	public ZTreeBean userRole(@PathVariable String groupId){
		//获取所有的角色
		List<SysRole> allRoles = roleService.querySysRoleByCondition(null, null, null, PublicEnum.NORMAL.value());
		//获取当前用户的角色
		List<SysRole> selectedRoles = roleService.getSysRoleByGroupId(groupId, PublicEnum.NORMAL.value());
		List<ZTreeBean> beans=new ArrayList<ZTreeBean>();
		for (SysRole sysRole : allRoles) {
			boolean checked=false;
			for (SysRole selectedRole : selectedRoles) {
				if(selectedRole.getRoleId()==sysRole.getRoleId()){
					checked=true;
					break;
				}
			}
			beans.add(new ZTreeBean(String.valueOf(sysRole.getRoleId()), "topRole", sysRole.getRoleName(),true,"",checked));
		}
		ZTreeBean bean=new ZTreeBean("topRole", "0", "角色树", true, "", "", false, beans);
		return bean;
	}
	
	/**
	 * 用户组分配角色
	 * @param groupId 用户组id
	 * @param roleIds 角色id集合
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:group:allowRole")
	@RequestMapping(value={"/groupRole"},method=RequestMethod.POST)
	public ResponseEntity saveGroupRole(String groupId,String roleIds){
		try {
			groupRoleService.saveGroupRole(groupId, roleIds, SecurityUtils.getSubject().getPrincipal().toString());
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "用户组角色保存出现异常", null);
		}
		return new ResponseEntity(200, "用户组角色保存成功", null);
	}
}
