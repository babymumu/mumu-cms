package com.lovecws.mumu.controller.system.log;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
/**
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:19:19
 */
public class LogMessageSender {
	@Autowired
	@Qualifier("jmsTopicTemplate")
	private JmsTemplate topicJmsTemplate;
	
	@Autowired
	@Qualifier("jmsQueueTemplate")
	private JmsTemplate queueJmsTemplate;

	private String queueName;//队列名称
	private String topicName;//主题名称
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	/**
	 * 发送一条消息到指定的订阅者（目标）
	 * @param topicName 订阅者名称
	 * @param message 消息内容
	 */
	public void sendTopicMessage(final Serializable message) {
		topicJmsTemplate.send(topicName, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(message);
			}
		});
	}
	
	/**
	 * 发送一条消息到指定的队列（目标）
	 * @param queueName 队列名称
	 * @param message 消息内容
	 */
	public void sendQueueMessage(final Serializable message) {
		queueJmsTemplate.send(queueName, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(message);
			}
		});
	}
}
