package com.lovecws.mumu.controller.common.sms;

import com.lovecws.mumu.common.core.response.HttpCode;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.common.core.utils.ValidateUtils;
import com.lovecws.mumu.common.sms.exception.SMSException;
import com.lovecws.mumu.common.sms.service.JPushSMSService;
import com.lovecws.mumu.system.entity.SysUser;
import com.lovecws.mumu.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ganliang on 2017/3/8.
 */
@Controller
@RequestMapping("/common/sms")
public class SMSController {

    @Autowired
    private JPushSMSService smsService;
    @Autowired
    private SysUserService userService;

    /**
     * 发送短信消息
     * @param userPhone 用户手机号码
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/send",method = RequestMethod.POST)
    public ResponseEntity sendSMSMessage(String userPhone){
        //判断手机号码是否非空
        if(StringUtil.isEmpty(userPhone)|| !ValidateUtils.isPhoneNumber(userPhone)){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"手机号码格式不正确");
        }
        try{
           String smsCode=smsService.sendSMS(userPhone);
            return new ResponseEntity(HttpCode.OK,smsCode);
        }catch (SMSException e){
            e.printStackTrace();
            return new ResponseEntity(HttpCode.SERVER_ERROR,"sms短信服务器出现异常");
        }
    }

    /**
     * 修改密码时，验证用户手机号码
     * @param userPhone 用户手机号码
     * @param smsCode sms code
     * @param verifyCode sms验证码
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/verify",method = RequestMethod.POST)
    public ResponseEntity verifySMSMessage(String userPhone,String smsCode,String verifyCode){
        //判断手机号码是否非空
        if(StringUtil.isEmpty(userPhone)|| !ValidateUtils.isPhoneNumber(userPhone)){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"手机号码格式不正确");
        }
        //判断短信验证码是否为空
        if(StringUtil.isEmpty(smsCode)||StringUtil.isEmpty(verifyCode)){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"短信验证码不能为空");
        }
        //根据手机号码获取用户信息
        SysUser user=userService.getUserByUserPhone(userPhone);
        if(user==null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"手机号码未注册!");
        }
        try{
            boolean success=smsService.validateMessage(smsCode,verifyCode);
            if(success){
                return new ResponseEntity(HttpCode.OK,user.getUserId());
            }
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"短信验证码验证失败,请重新发送短信验证码");
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity(HttpCode.SERVER_ERROR,"验证短信信息服务器出现异常");
        }
    }

    /**
     * 用户注册时，验证用户手机号码
     * @param userPhone 用户手机号码
     * @param smsCode sms code
     * @param verifyCode sms验证码
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/verifyReg",method = RequestMethod.POST)
    public ResponseEntity verifyRegSMSMessage(String userPhone,String smsCode,String verifyCode){
        //判断手机号码是否非空
        if(StringUtil.isEmpty(userPhone)|| !ValidateUtils.isPhoneNumber(userPhone)){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"手机号码格式不正确");
        }
        //判断短信验证码是否为空
        if(StringUtil.isEmpty(smsCode)||StringUtil.isEmpty(verifyCode)){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"短信验证码不能为空");
        }
        //根据手机号码获取用户信息
        SysUser user=userService.getUserByUserPhone(userPhone);
        if(user!=null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"手机号码已注册!");
        }
        try{
            boolean success=smsService.validateMessage(smsCode,verifyCode);
            if(success){
                return new ResponseEntity(HttpCode.OK,null);
            }
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"短信验证码验证失败,请重新发送短信验证码");
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity(HttpCode.SERVER_ERROR,"验证短信信息服务器出现异常");
        }
    }
}
