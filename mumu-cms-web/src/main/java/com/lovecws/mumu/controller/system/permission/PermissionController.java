package com.lovecws.mumu.controller.system.permission;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.system.entity.SysMenu;
import com.lovecws.mumu.system.entity.SysPermission;
import com.lovecws.mumu.system.service.SysMenuService;
import com.lovecws.mumu.system.service.SysPermissionService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/system/permission")
public class PermissionController {

	private static final Logger log=Logger.getLogger(PermissionController.class);
	@Autowired
	private SysPermissionService permissionService;
	@Autowired
	private SysMenuService menuService;
	
	/**
	 * 权限列表
	 * @return
	 */
	@RequiresPermissions("system:permission:view")
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(HttpServletRequest request){
		List<SysMenu> subMenus = menuService.getSubSysMenu(PublicEnum.NORMAL.value());
		request.setAttribute("subMenus", subMenus);
		return "system/permission/list";
	}
	
	/**
	 * 菜单列表
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:permission:view")
	@RequestMapping(value={"/page"})
	public Map<String,Object> page(String menuId,String permissionCode,String permissionName,int beginIndex,int pageSize,HttpServletRequest request){
		// 分页查询
		PageBean<SysPermission> pageBean = permissionService.listPage(menuId,permissionCode,permissionName, beginIndex / pageSize + 1, pageSize);
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("total", pageBean.getTotalCount());
		page.put("rows", pageBean.getRecordList());
		return page;
	}
	
	/**
	 * 添加权限
	 * @return
	 */
	@RequiresPermissions("system:permission:add")
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(HttpServletRequest request){
		List<SysMenu> subMenus = menuService.getSubSysMenu(PublicEnum.NORMAL.value());
		request.setAttribute("subMenus", subMenus);
		return "system/permission/add";
	}
	
	/**
	 * 添加权限
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:permission:add")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity savePermission(SysPermission permission, HttpServletRequest request){
		//获取权限内码和权限名称下面的权限列表
		List<SysPermission> permissions = permissionService.querySysPermissionByCondition(null, permission.getPermissionCode(), permission.getPermissionName(), PublicEnum.NORMAL.value());
		if(permissions!=null&&permissions.size()>0){
			return new ResponseEntity(500, "权限内码、权限名称不可重复", null);
		}
		try {
			permission.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
			permissionService.addPermission(permission);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500,"保存权限内码出现异常",null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 查看权限详情
	 * @param permissionId 权限id
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:permission:view")
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String viewPermission(String permissionId,HttpServletRequest request){
		SysPermission permission = permissionService.getSysPermissionById(permissionId);
		request.setAttribute("permission",permission);
		return "system/permission/view";
	}
	
	/**
	 * 编辑权限
	 * @param permissionId 权限id
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:permission:edit")
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public String editPermission(String permissionId,HttpServletRequest request){
		List<SysMenu> subMenus = menuService.getSubSysMenu(PublicEnum.NORMAL.value());
		request.setAttribute("subMenus", subMenus);
		
		SysPermission permission = permissionService.getSysPermissionById(permissionId);
		request.setAttribute("permission",permission);
		return "system/permission/edit";
	}
	
	/**
	 * 编辑权限
	 * @param permissionId 权限id
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:permission:edit")
	@RequestMapping(value="/edit",method=RequestMethod.PUT)
	public ResponseEntity udatePermission(SysPermission permission,HttpServletRequest request){
		//获取权限内码和权限名称下面的权限列表
		List<SysPermission> permissions = permissionService.querySysPermissionByCondition(null, permission.getPermissionCode(), permission.getPermissionName(), PublicEnum.NORMAL.value());
		if(permissions!=null&&permissions.size()>1){
			return new ResponseEntity(500, "权限内码、权限名称不可重复", null);
		}
		if(permissions!=null&&permissions.size()==1){
			if(permission.getPermissionId()!=null&&permissions.get(0).getPermissionId()!=permission.getPermissionId()){
				return new ResponseEntity(500, "权限内码、权限名称不可重复", null);
			}
		}
		try {
			permission.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
			permissionService.updatePermissionById(permission);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "更新权限出现异常", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 删除权限
	 * @param permissionId 权限id
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:permission:delete")
	@RequestMapping(value="/delete",method=RequestMethod.DELETE)
	public ResponseEntity delete(String permissionId){
		try {
			permissionService.deletePermissionById(permissionId);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "删除菜单权限出现异常", null);
		}
		return new ResponseEntity();
	}
}
