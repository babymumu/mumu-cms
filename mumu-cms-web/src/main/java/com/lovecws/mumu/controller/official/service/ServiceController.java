package com.lovecws.mumu.controller.official.service;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.official.entity.OfficialServiceCategoryEntity;
import com.lovecws.mumu.official.entity.OfficialServiceEntity;
import com.lovecws.mumu.official.service.OfficialServiceCategoryService;
import com.lovecws.mumu.official.service.OfficialServiceService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ganliang on 2017/3/13.
 * 服务集合控制器
 */
@Controller
@RequestMapping("/official/service")
public class ServiceController {

    @Autowired
    private OfficialServiceService serviceService;
    @Autowired
    private OfficialServiceCategoryService serviceCategoryService;


    /**
     *
     * @return
     */
    @RequiresPermissions(value = {"official:service:view","official:service:categoryView"},logical= Logical.OR)
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(){
        return "official/service/list";
    }

    /**
     * 服务分类
     * @return
     */
    @RequiresPermissions("official:service:view")
    @RequestMapping(value = "/services", method = RequestMethod.GET)
    public String services(){
        return "official/service/services";
    }

    /**
     *分页获取服务列表
     * @param searchContent 搜索内容(标题或者分类id)
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:view")
    @RequestMapping(value = "/servicesPage", method = RequestMethod.GET)
    public Map<String,Object> servicePage(String searchContent, int beginIndex, int pageSize){
        PageBean<OfficialServiceEntity> pageBean=serviceService.listPage(searchContent,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 跳转到添加服务列表页面
     * @return
     */
    @RequiresPermissions("official:service:add")
    @RequestMapping(value = "/servicesAdd", method = RequestMethod.GET)
    public String servicesAdd(HttpServletRequest request){
        List<OfficialServiceCategoryEntity> serviceCategorys=serviceCategoryService.getServiceCategoryByCondition(null);
        request.setAttribute("serviceCategorys",serviceCategorys);
        return "official/service/servicesAdd";
    }

    /**
     * 添加服务
     * @param title 服务标题
     * @param categoryId 服务分类
     * @param logo 服务logo
     * @param content 服务内容
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:add")
    @RequestMapping(value = "/servicesAdd", method = RequestMethod.POST)
    public ResponseEntity saveServices(String title,int categoryId,String logo,String content){
        //校验参数
        if(StringUtil.isEmpty(title)){
            return new ResponseEntity(400,"服务标题不能为空!",null);
        }
        if(StringUtil.isEmpty(logo)){
            return new ResponseEntity(400,"服务logo图标不能为空!",null);
        }
        OfficialServiceEntity service=new OfficialServiceEntity();
        service.setCreateTime(new Date());
        service.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        service.setStatus(PublicEnum.NORMAL.value());
        service.setTitle(title);
        service.setCategoryId(categoryId);
        service.setLogo(logo);
        service.setContent(content);
        serviceService.addService(service);
        return new ResponseEntity(200,"保存服务操作成功!",null);
    }

    /**
     * 查看服务分类详情
     * @param serviceId 服务id
     * @param request
     * @return
     */
    @RequiresPermissions("official:service:view")
    @RequestMapping(value = "/servicesView/{serviceId}", method = RequestMethod.GET)
    public String servicesView(@PathVariable int serviceId, HttpServletRequest request){
        OfficialServiceEntity service=serviceService.getServiceById(serviceId);
        request.setAttribute("service",service);
        return "official/service/servicesView";
    }

    /**
     * 跳转到编辑服务页面
     * @param serviceId 服务id
     * @param request
     * @return
     */
    @RequiresPermissions("official:service:edit")
    @RequestMapping(value = "/servicesEdit/{serviceId}", method = RequestMethod.GET)
    public String servicesEdit(@PathVariable int serviceId, HttpServletRequest request){
        OfficialServiceEntity service=serviceService.getServiceById(serviceId);
        request.setAttribute("service",service);

        List<OfficialServiceCategoryEntity> serviceCategorys=serviceCategoryService.getServiceCategoryByCondition(null);
        request.setAttribute("serviceCategorys",serviceCategorys);
        return "official/service/servicesEdit";
    }

    /**
     * 更新服务
     * @param serviceId
     * @param title
     * @param categoryId
     * @param logo
     * @param content
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:edit")
    @RequestMapping(value = "/servicesEdit", method = RequestMethod.POST)
    public ResponseEntity servicesEdit(int serviceId,String title,int categoryId,String logo,String content){
        OfficialServiceEntity serviceEntity=serviceService.getServiceById(serviceId);
        if(serviceEntity==null){
            return new ResponseEntity(400,"服务不存在",null);
        }
        OfficialServiceEntity service=new OfficialServiceEntity();
        service.setId(serviceId);
        service.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
        service.setEditTime(new Date());
        service.setTitle(title);
        service.setCategoryId(categoryId);
        service.setLogo(logo);
        service.setContent(content);
        serviceService.updateService(service);
        return new ResponseEntity(200,"保存服务操作成功!",null);
    }

    /**
     * 删除服务
     * @param serviceId 服务id
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:service:delete")
    @RequestMapping(value = "/servicesDelete/{serviceId}", method = RequestMethod.DELETE)
    public ResponseEntity servicesDelete(@PathVariable int serviceId){
        OfficialServiceEntity service=serviceService.getServiceById(serviceId);
        if(service==null){
            return new ResponseEntity(400,"服务不存在",null);
        }
        serviceService.deleteServiceById(serviceId);
        return new ResponseEntity(200,"删除服务操作成功",null);
    }

    /**
     * 获取只是显示的服务列表
     * @param categoryId 服务分类id
     * @param request
     * @return
     */
    @RequiresPermissions("official:service:view")
    @RequestMapping(value = "/servicesList/{categoryId}", method = RequestMethod.GET)
    public String servicesList(@PathVariable int categoryId,HttpServletRequest request){
        request.setAttribute("cid",categoryId);
        return "official/service/servicesList";
    }
}
