package com.lovecws.mumu.controller.official.message;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.official.entity.OfficialMessageEntity;
import com.lovecws.mumu.official.service.OfficialMessageService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ganliang on 2017/3/13.
 * 用户留言集合控制器
 */
@Controller
@RequestMapping("/official/message")
public class MessageController {

    @Autowired
    private OfficialMessageService messageService;

    @RequiresPermissions("official:message:view")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(){
        return "official/message/list";
    }

    /**
     * 分页获取用户留言列表
     * @param searchContent 搜索内容(标题或者分类id)
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:message:view")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Map<String,Object> page(String searchContent, int beginIndex, int pageSize){
        PageBean<OfficialMessageEntity> pageBean=messageService.listPage(searchContent,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 查看用户留言分类详情
     * @param messageId 用户留言id
     * @param request
     * @return
     */
    @RequiresPermissions("official:message:view")
    @RequestMapping(value = "/view/{messageId}", method = RequestMethod.GET)
    public String productsView(@PathVariable int messageId, HttpServletRequest request){
        OfficialMessageEntity message=messageService.getMessageIdById(messageId);
        request.setAttribute("message",message);
        return "official/message/view";
    }

    /**
     * 删除用户留言
     * @param messageId 用户留言id
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:message:delete")
    @RequestMapping(value = "/delete/{messageId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable int messageId){
        OfficialMessageEntity message=messageService.getMessageIdById(messageId);
        if(message==null){
            return new ResponseEntity(400,"用户留言不存在",null);
        }
        messageService.deleteMessageById(messageId);
        return new ResponseEntity(200,"删除用户留言操作成功",null);
    }

    /**
     * 添加用户留言
     * @param userName 用户名称
     * @param userEmail 用户邮箱
     * @param userPhone 用户手机号码
     * @param title 标题
     * @param content 内容
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity add(String userName,String userEmail,String userPhone,String title,String content){
        OfficialMessageEntity message=new OfficialMessageEntity();
        message.setStatus(PublicEnum.NORMAL.value());
        message.setCreateTime(new Date());
        message.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        message.setUserName(userName);
        message.setUserPhone(userPhone);
        message.setUserEmail(userEmail);
        message.setTitle(title);
        message.setContent(content);
        messageService.addMessage(message);
        return new ResponseEntity(200,"添加用户留言操作成功",null);
    }
}
