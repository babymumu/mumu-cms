package com.lovecws.mumu.controller.portal.account;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.response.HttpCode;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.shiro.entity.BaseRealm;
import com.lovecws.mumu.shiro.utils.PasswordHelper;
import com.lovecws.mumu.system.entity.SysUser;
import com.lovecws.mumu.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping("/portal/account")
public class RegisterController {

	@Autowired
	private SysUserService userService;
	/**
	 * 跳转到注册列表界面
	 * @return
	 */
	@RequestMapping(value={"/reg"},method=RequestMethod.GET)
	public String reg(){
		return "portal/account/reg";
	}
	
	/**
	 * 跳转到邮箱注册界面
	 * @return
	 */
	@RequestMapping(value={"/regEmail"},method=RequestMethod.GET)
	public String regEmail(){
		return "portal/account/regEmail";
	}
	
	/**
	 * 验证邮箱后 跳转到注册页面
	 * @return
	 */
	@RequestMapping(value={"/regEmailDetail"},method={RequestMethod.POST,RequestMethod.GET})
	public String regEmailDetail(String email,String regToken,HttpServletRequest request){
		request.setAttribute("email",email);
		request.setAttribute("regToken",regToken);
		return "portal/account/regEmailDetail";
	}
	
	/**
	 * 跳转到手机号码注册页面
	 * @return
	 */
	@RequestMapping(value={"/regPhone"},method=RequestMethod.GET)
	public String regPhone(){
		return "portal/account/regPhone";
	}
	
	/**
	 * 跳转到手机号码注册详情
	 * @param userPhone 手机号码
	 * @return
	 */
	@RequestMapping(value={"/regPhoneDetail"},method={RequestMethod.POST,RequestMethod.GET})
	public String regPhoneDetail(String userPhone,HttpServletRequest request){
		request.setAttribute("userPhone",userPhone);
		return "portal/account/regPhoneDetail";
	}
	
	/**
	 * 使用手机号码注册成功 中转页面(跳转到登录页面)
	 * @return
	 */
	@RequestMapping(value={"/register"},method=RequestMethod.POST)
	public String register(String userPhone,String verifyCode){
		return "portal/account/regSuccess";
	}

	/**
	 * 验证注册用户信息
	 * @param userPhone 用户手机号码
	 * @param userName 用户名称
	 * @param userEmail 用户邮箱
 	 * @param password 用户明文密码
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value={"/verifyReg"},method=RequestMethod.POST)
	public ResponseEntity verifyReg(String userPhone, String userName,String userEmail,String password,String captchaCode,HttpServletRequest request){
		//判断验证码是否校验成功
		if(!StringUtil.isEmpty(captchaCode)){
			String shiroLoginFailure=(String) request.getAttribute("shiroLoginFailure");
			if(shiroLoginFailure!=null){
				return new ResponseEntity(HttpCode.PARAMETER_ERROR,"验证码不正确!");
			}
		}
		//查看注册用户是否已经存在
		SysUser sysUser=userService.getUserByCondition(userName,userPhone,userEmail);
		if(sysUser!=null){
			if(userPhone!=null&&userPhone.equals(sysUser.getPhone())){
				return new ResponseEntity(HttpCode.PARAMETER_ERROR,"该手机号码已经注册,请更换手机号码!");
			}
			if(userName!=null&&userName.equals(sysUser.getUserName())){
				return new ResponseEntity(HttpCode.PARAMETER_ERROR,"该用户名称已经注册,请更换用户名称!");
			}
			if(userEmail!=null&&userEmail.equals(sysUser.getEmail())){
				return new ResponseEntity(HttpCode.PARAMETER_ERROR,"该邮箱已经注册,请更换邮箱地址!");
			}

		}

		SysUser user=new SysUser();
		user.setUserName(userName);
		user.setCreator(userName);
		user.setCreateTime(new Date());
		user.setEmail(userEmail);
		if(StringUtil.isEmpty(userEmail)){
			user.setEmailActive("0");
		}else{
			user.setEmailActive("1");
		}
		user.setPhone(userPhone);
		if(StringUtil.isEmpty(userPhone)){
			user.setPhoneActive("0");
		}else{
			user.setPhoneActive("1");
		}
		//加密密码
		BaseRealm baseRealm = PasswordHelper.encryptPassword(new BaseRealm(userName, password));
		user.setPassword(baseRealm.getPassword());
		user.setSalt(baseRealm.getSalt());

		user.setType("common");
		user.setUserStatus(PublicEnum.NORMAL.value());
		userService.addUser(user);
		return new ResponseEntity(HttpCode.OK,user);
	}
}
