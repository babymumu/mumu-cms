package com.lovecws.mumu.controller.common.email;

import com.lovecws.mumu.common.core.response.HttpCode;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.email.service.SimpleEmailService;
import com.lovecws.mumu.system.entity.SysUser;
import com.lovecws.mumu.system.service.SysUserService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/8.
 */
@Controller
@RequestMapping("/common/email/")
public class SimpleEmailController {

    @Autowired
    private SysUserService userService;
    @Autowired
    private SimpleEmailService emailService;
    //velocity模板引擎
    @Autowired
    private VelocityEngine velocityEngine;

    /**
     * 邮件发送（找回密码）
     * @param email 邮箱号码
     * @return
     */
    @ResponseBody
    @RequestMapping(value={"/sendPwdEmail"},method= RequestMethod.POST)
    public ResponseEntity sendPwdEmail(String email, HttpServletRequest request){
        //判断验证码是否校验成功
        String shiroLoginFailure=(String) request.getAttribute("shiroLoginFailure");
        if(shiroLoginFailure!=null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"验证码不正确!");
        }
        SysUser sysUser=userService.getUserByCondition(null,null,email);
        if(sysUser==null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"该邮箱未注册系统!");
        }
        //发送邮件
        try {
            String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+ request.getContextPath()+"/";
            Map<String,Object> model=new HashMap<String,Object>();
            model.put("userName",sysUser.getUserName());
            model.put("logoURL",basePath+"resources/cws/images/logo.png");
            model.put("loginURL",basePath+"portal/account/logining");
            model.put("iforgotURL",basePath+"portal/account/pwdDetail");
            model.put("email",email);
            model.put("title","慕慕CMS管理系统【找回密码】");
            String content= VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "tpl/pwdEmail.html","UTF-8",model);
            System.out.println(content);
            boolean sendSuccess=emailService.send(email,null,"慕慕CMS管理系统【找回密码】",content);
            if(sendSuccess){
                return new ResponseEntity(HttpCode.OK,"邮件发送成功!");
            }
            return new ResponseEntity(HttpCode.SERVER_ERROR,"邮件发送失败!");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(HttpCode.SERVER_ERROR,"邮件发送时,服务器出现异常!");
        }
    }

    /**
     * 邮件发送（注册）
     * @param email 邮箱号码
     * @return
     */
    @ResponseBody
    @RequestMapping(value={"/sendRegEmail"},method= RequestMethod.POST)
    public ResponseEntity sendRegEmail(String email, HttpServletRequest request){
        //判断验证码是否校验成功
        String shiroLoginFailure=(String) request.getAttribute("shiroLoginFailure");
        if(shiroLoginFailure!=null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"验证码不正确!");
        }
        SysUser sysUser=userService.getUserByCondition(null,null,email);
        if(sysUser!=null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"该邮箱已注册系统!");
        }
        //发送邮件
        try {
            String regToken= String.valueOf(RandomUtils.nextInt(100000, 999999));
            String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+ request.getContextPath()+"/";
            Map<String,Object> model=new HashMap<String,Object>();
            model.put("logoURL",basePath+"resources/cws/images/logo2.png");
            model.put("loginURL",basePath+"portal/account/logining");
            model.put("regEmailUrl",basePath+"portal/account/regEmailDetail");
            model.put("email",email);
            model.put("title","慕慕CMS管理系统【注册码】");
            model.put("regToken",regToken);
            String content= VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "tpl/regEmail.html","UTF-8",model);
            System.out.println(content);
            boolean sendSuccess=emailService.send(email,null,"慕慕CMS管理系统【注册码】",content);
            if(sendSuccess){
                return new ResponseEntity(HttpCode.OK,regToken);
            }
            return new ResponseEntity(HttpCode.SERVER_ERROR,"邮件发送失败!");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(HttpCode.SERVER_ERROR,"邮件发送时,服务器出现异常!");
        }
    }
}
