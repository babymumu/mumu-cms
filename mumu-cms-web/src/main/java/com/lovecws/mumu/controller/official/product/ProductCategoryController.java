package com.lovecws.mumu.controller.official.product;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.official.entity.OfficialProductCategoryEntity;
import com.lovecws.mumu.official.service.OfficialProductCategoryService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/13.
 */
@Controller
@RequestMapping("/official/product")
public class ProductCategoryController {

    @Autowired
    private OfficialProductCategoryService productCategoryService;

    /**
     * 产品分类
     * @return
     */
    @RequiresPermissions(value = {"official:product:categoryView"})
    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String productCategory(){
        return "official/product/category";
    }

    /**
     *分页获取产品分类数据
     * @param categoryName 产品分类名称
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:categoryView"})
    @RequestMapping(value = "/categoryPage", method = RequestMethod.GET)
    public Map<String,Object> productCategoryPage(String categoryName,int beginIndex,int pageSize){
        PageBean<OfficialProductCategoryEntity> pageBean=productCategoryService.listPage(categoryName,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 跳转到添加产品分类页面
     * @return
     */@RequiresPermissions(value = {"official:product:categoryAdd"})
    @RequestMapping(value = "/categoryAdd", method = RequestMethod.GET)
    public String productCategoryAdd(){
        return "official/product/categoryAdd";
    }

    /**
     * 添加产品分类
     * @param categoryName 产品分类名称
     * @param remark 产品分类描述
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:categoryAdd"})
    @RequestMapping(value = "/categoryAdd", method = RequestMethod.POST)
    public ResponseEntity saveProductCategory(String categoryName,String remark){
        //校验参数
        if(StringUtil.isEmpty(categoryName)){
            return new ResponseEntity(400,"产品分类不能为空!",null);
        }
        OfficialProductCategoryEntity productCategory=new OfficialProductCategoryEntity();
        productCategory.setCreateTime(new Date());
        productCategory.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        productCategory.setCategoryName(categoryName);
        productCategory.setStatus(PublicEnum.NORMAL.value());
        productCategory.setRemark(remark);
        productCategoryService.addProductCategory(productCategory);
        return new ResponseEntity(200,"保存产品分类操作成功!",null);
    }

    /**
     * 查看产品分类详情
     * @param categoryId 产品分类id
     * @param request
     * @return
     */
    @RequiresPermissions(value = {"official:product:categoryView"})
    @RequestMapping(value = "/categoryView/{categoryId}", method = RequestMethod.GET)
    public String productCategoryView(@PathVariable int categoryId, HttpServletRequest request){
        OfficialProductCategoryEntity productCategoryEntity=productCategoryService.getProductCategoryById(categoryId);
        request.setAttribute("productCategory",productCategoryEntity);
        return "official/product/categoryView";
    }

    /**
     * 跳转到编辑产品分类页面
     * @param categoryId 产品分类id
     * @param request
     * @return
     */
    @RequiresPermissions(value = {"official:product:categoryEdit"})
    @RequestMapping(value = "/categoryEdit/{categoryId}", method = RequestMethod.GET)
    public String productCategoryEdit(@PathVariable int categoryId, HttpServletRequest request){
        OfficialProductCategoryEntity productCategoryEntity=productCategoryService.getProductCategoryById(categoryId);
        request.setAttribute("productCategory",productCategoryEntity);
        return "official/product/categoryEdit";
    }

    /**
     * 更新产品分类
     * @param categoryId 产品分类id
     * @param categoryName 产品分类名称
     * @param remark 产品分类描述
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:categoryEdit"})
    @RequestMapping(value = "/categoryEdit", method = RequestMethod.POST)
    public ResponseEntity updateProductCategory(int categoryId,String categoryName,String remark){
        OfficialProductCategoryEntity productCategoryEntity=productCategoryService.getProductCategoryById(categoryId);
        if(productCategoryEntity==null){
            return new ResponseEntity(400,"产品分类不存在",null);
        }
        OfficialProductCategoryEntity productCategory=new OfficialProductCategoryEntity();
        productCategory.setId(productCategoryEntity.getId());
        productCategory.setCategoryName(categoryName);
        productCategory.setRemark(remark);
        productCategory.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
        productCategory.setEditTime(new Date());
        productCategoryService.updateProductCategory(productCategory);
        return new ResponseEntity(200,"保存产品分类操作成功!",null);
    }

    /**
     * 删除产品分类
     * @param categoryId 产品分类id
     * @return
     */
    @ResponseBody
    @RequiresPermissions(value = {"official:product:categoryDelete"})
    @RequestMapping(value = "/categoryDelete/{categoryId}", method = RequestMethod.DELETE)
    public ResponseEntity updateProductCategory(@PathVariable int categoryId){
        OfficialProductCategoryEntity productCategoryEntity=productCategoryService.getProductCategoryById(categoryId);
        if(productCategoryEntity==null){
            return new ResponseEntity(400,"产品分类不存在",null);
        }
        productCategoryService.deleteProductCategoryById(categoryId);
        return new ResponseEntity(200,"删除产品分类操作成功",null);
    }
}
