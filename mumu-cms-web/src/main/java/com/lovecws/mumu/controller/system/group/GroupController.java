package com.lovecws.mumu.controller.system.group;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.system.entity.SysGroup;
import com.lovecws.mumu.system.entity.SysOrganize;
import com.lovecws.mumu.system.service.SysGroupService;
import com.lovecws.mumu.system.service.SysOrganizeService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/group")
public class GroupController {
	
	private static final Logger log=Logger.getLogger(GroupController.class);
	@Autowired
	private SysGroupService groupService;
	@Autowired
	private SysOrganizeService organizeService;
	
	/**
	 * 用户组列表
	 * @return
	 */
	@RequiresPermissions("system:group:view")
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(HttpServletRequest request){
		return "system/group/list";
	}
	
	/**
	 * 分页获取用户组列表
	 * @param orgId 组织id
	 * @param beginIndex 开始索引
	 * @param pageSize 一页数量
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:group:view")
	@RequestMapping(value={"/page"})
	public Map<String,Object> page(String orgId,String groupName,int beginIndex,int pageSize){
		// 分页查询
		PageBean<SysGroup> pageBean = groupService.listPage(orgId,groupName, beginIndex / pageSize + 1, pageSize);
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("total", pageBean.getTotalCount());
		page.put("rows", pageBean.getRecordList());
	    return page;
	}
	
	/**
	 * 添加用户组
	 * @return
	 */
	@RequiresPermissions("system:group:add")
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(HttpServletRequest request){
		List<SysOrganize> organizes = organizeService.querySysOrganizeByCondition(null);
		request.setAttribute("organizes", organizes);
		return "system/group/add";
	}
	
	/**
	 * 保存用户组
	 * @param orgId 机构组织id
	 * @param groupName 用户组名称
	 * @param remark 描述
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:group:add")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity saveSysGroup(int orgId,String groupName,String remark){
		List<SysGroup> groups=groupService.querySysGroupByCondition(null,groupName);
		if(groups!=null&&groups.size()>0){
			return new ResponseEntity(500, "用户组名称不能重复!", null);
		}
		try {
			SysGroup group=new SysGroup();
			group.setGroupName(groupName);
			group.setOrgId(orgId);
			group.setRemark(remark);
			group.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
			groupService.addSysGroup(group);
			return new ResponseEntity(200, "用户组添加成功", null);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "保存用户组出现异常!", null);
		}
	}
	
	/**
	 * 查看用户组详情
	 * @param groupId 用户组id
	 * @return
	 */
	@RequiresPermissions("system:group:view")
	@RequestMapping(value="/view/{groupId}",method=RequestMethod.GET)
	public String view(@PathVariable String groupId,HttpServletRequest request){
		SysGroup group=groupService.getSysGroupById(groupId);
		request.setAttribute("group", group);
		return "system/group/view";
	}
	
	/**
	 * 编辑用户组
	 * @param groupId 用户组id
	 * @return
	 */
	@RequiresPermissions("system:group:edit")
	@RequestMapping(value="/edit/{groupId}",method=RequestMethod.GET)
	public String edit(@PathVariable String groupId,HttpServletRequest request){
		SysGroup group=groupService.getSysGroupById(groupId);
		request.setAttribute("group", group);
		
		List<SysOrganize> organizes = organizeService.querySysOrganizeByCondition(null);
		request.setAttribute("organizes", organizes);
		return "system/group/edit";
	}
	
	/**
	 * 更新用户组
	 * @param groupId 用户id
	 * @param orgId 机构id
	 * @param groupName 用户组id
	 * @param remark 描述
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:group:edit")
	@RequestMapping(value="/edit",method=RequestMethod.PUT)
	public ResponseEntity updateSysGroup(int groupId,int orgId,String groupName,String remark){
		List<SysGroup> groups=groupService.querySysGroupByCondition(null,groupName);
		if(groups!=null&&groups.size()>0&&groups.get(0).getGroupId()!=groupId){
			return new ResponseEntity(500, "用户组名称不能重复!", null);
		}
		try {
			SysGroup group=new SysGroup();
			group.setGroupId(groupId);
			group.setOrgId(orgId);
			group.setGroupName(groupName);
			group.setRemark(remark);
			group.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
			groupService.updateSysGroupById(group);
			return new ResponseEntity(200, "用户组更新成功", null);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "更新用户组出现异常!", null);
		}
	}
	
	@ResponseBody
	@RequiresPermissions("system:group:delete")
	@RequestMapping(value="/delete/{groupId}",method=RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable String groupId){
		try {
			groupService.deleteSysGroupById(groupId);
			return new ResponseEntity(200,"删除用户组成功",null);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "更新用户组出现异常!", null);
		}
	}
}
