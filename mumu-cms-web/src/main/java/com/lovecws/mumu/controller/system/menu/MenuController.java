 package com.lovecws.mumu.controller.system.menu;

 import com.lovecws.mumu.common.core.enums.PublicEnum;
 import com.lovecws.mumu.common.core.page.PageBean;
 import com.lovecws.mumu.common.core.response.ResponseEntity;
 import com.lovecws.mumu.system.entity.SysMenu;
 import com.lovecws.mumu.system.service.SysMenuService;
 import org.apache.log4j.Logger;
 import org.apache.shiro.authz.annotation.RequiresPermissions;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Controller;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestMethod;
 import org.springframework.web.bind.annotation.ResponseBody;

 import javax.servlet.http.HttpServletRequest;
 import java.util.HashMap;
 import java.util.Iterator;
 import java.util.List;
 import java.util.Map;

@Controller
@RequestMapping("/system/menu")
public class MenuController {

	private static final Logger log=Logger.getLogger(MenuController.class);
	
	@Autowired
	private SysMenuService menuService;
	
	/**
	 * 菜单列表
	 * @return
	 */
	@RequiresPermissions("system:menu:view")
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(HttpServletRequest request){
		List<SysMenu> topMenus = menuService.getTopSysMenu(PublicEnum.NORMAL.value());
		request.setAttribute("topMenus", topMenus);
		return "system/menu/list";
	}
	
	/**
	 * 分页获取菜单列表
	 * @param parentMenuId 父菜单id
	 * @param menuName 菜单名称
	 * @param beginIndex 开始索引
	 * @param pageSize 一页数量
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:view")
	@RequestMapping(value={"/page"})
	public Map<String,Object> page(String parentMenuId,String menuName,int beginIndex,int pageSize){
		if("0".equals(parentMenuId)){
			parentMenuId=null;
		}
		// 分页查询
		PageBean<SysMenu> pageBean = menuService.listPage(parentMenuId,menuName, beginIndex / pageSize + 1, pageSize);
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("total", pageBean.getTotalCount());
		page.put("rows", pageBean.getRecordList());
	    return page;
	}
	
	/**
	 * 添加菜单
	 * @return
	 */
	@RequiresPermissions("system:menu:add")
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(HttpServletRequest request){
		List<SysMenu> topMenus = menuService.getTopSysMenu(PublicEnum.NORMAL.value());
		request.setAttribute("topMenus", topMenus);
		return "system/menu/add";
	}
	
	/**
	 * 检测菜单内码和菜单名称是否已经存在
	 * @param menuCode 菜单内码
	 * @param menuName 菜单名称
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/exists",method=RequestMethod.GET)
	public ResponseEntity exists(String menuCode,String menuName){
		try {
			List<SysMenu> menus=menuService.querySysMenuByCondition(null,menuCode,menuName);
			if(menus!=null&&menus.size()>0){
				return new ResponseEntity(500, "菜单内码、菜单名称不可重复", null);
			}
			return new ResponseEntity();
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "检测菜单内码、菜单名称是否存在出现异常", null);
		}
	}
	
	/**
	 * 保存菜单
	 * @param menu 菜单实体
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:add")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity saveMenu(SysMenu menu){
		try {
			menuService.addMenu(menu);
			return new ResponseEntity();
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "保存菜单出现异常", null);
		}
	}
	
	/**
	 * 编辑菜单
	 * @param menuId 菜单id
	 * @return
	 */
	@RequiresPermissions("system:menu:view")
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String view(String menuId,HttpServletRequest request){
		SysMenu menu = menuService.getSysMenuById(menuId);
		request.setAttribute("menu", menu);
		return "system/menu/view";
	}
	
	/**
	 * 编辑菜单
	 * @param menuId 菜单id
	 * @return
	 */
	@RequiresPermissions("system:menu:edit")
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public String edit(String menuId,HttpServletRequest request){
		SysMenu menu = menuService.getSysMenuById(menuId);
		request.setAttribute("menu", menu);
		
		List<SysMenu> topMenus = menuService.getTopSysMenu(PublicEnum.NORMAL.value());
		Iterator<SysMenu> iterator = topMenus.iterator();
		while(iterator.hasNext()){
			SysMenu sysMenu = iterator.next();
			if(sysMenu.getMenuId()==Integer.parseInt(menuId)){
				iterator.remove();
			}
		}
		request.setAttribute("topMenus", topMenus);
		return "system/menu/edit";
	}
	
	/**
	 * 更新菜单
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:edit")
	@RequestMapping(value="/edit",method=RequestMethod.PUT)
	public ResponseEntity updateMenu(SysMenu menu){
		try {
			menuService.updateMenuById(menu);
			return new ResponseEntity();
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "跟新菜单出现异常", "");
		}
	}
	
	/**
	 * 删除菜单
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:delete")
	@RequestMapping(value="/delete",method=RequestMethod.DELETE)
	public ResponseEntity delete(String menuId){
		try {
			menuService.deleteMenuById(menuId);
			return new ResponseEntity();
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "删除菜单出现异常", "");
		}
	}
	
	/**
	 * 子菜单
	 * @return
	 */
	@RequiresPermissions("system:menu:leaf")
	@RequestMapping(value="/subMenu",method=RequestMethod.GET)
	public String subMenu(String parentMenuId,HttpServletRequest request){
		request.setAttribute("parentMenuId", parentMenuId);
		return "system/menu/subMenu";
	}
	
	/**
	 * 子菜单
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:menu:leaf")
	@RequestMapping(value="/subMenuData",method=RequestMethod.GET)
	public Map<String, Object> subMenuData(String parentMenuId){
		List<SysMenu> menus=menuService.querySysMenuByCondition(parentMenuId,null,null);
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("total", menus.size());
		page.put("rows", menus);
		return page;
	}
}
