package com.lovecws.mumu.controller.portal.home;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.official.entity.OfficialNewsEntity;
import com.lovecws.mumu.official.entity.OfficialProductEntity;
import com.lovecws.mumu.official.entity.OfficialServiceEntity;
import com.lovecws.mumu.official.service.OfficialNewsService;
import com.lovecws.mumu.official.service.OfficialProductService;
import com.lovecws.mumu.official.service.OfficialServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/3/10.
 */
@Controller
@RequestMapping("/portal/home")
public class HomeController {

    @Autowired
    private OfficialProductService productService;
    @Autowired
    private OfficialServiceService serviceService;
    @Autowired
    private OfficialNewsService newsService;

    /**
     * 官网首页
     * @param request
     * @return
     */
    @RequestMapping("/index")
    public String home(HttpServletRequest request){
        //获取前三个服务
        PageBean<OfficialProductEntity> products=productService.listPage(null,1,3);
        request.setAttribute("products",products.getRecordList());
        //获取最近发布的服务
        PageBean<OfficialServiceEntity> services=serviceService.listPage(null,1,5);
        request.setAttribute("services",services.getRecordList());
        //获取最近的新闻
        PageBean<OfficialNewsEntity> news=newsService.listPage(null,1,6);
        request.setAttribute("news",news.getRecordList());
        return "portal/home/index";
    }

    /**
     * 获取产品列表
     * @param title 产品标题
     * @param currentPage 页数
     * @param pageSize 一页大小
     * @param request
     * @return
     */
    @RequestMapping("/product")
    public String product(String title, @RequestParam(required = false,defaultValue = "1") int currentPage, @RequestParam(required = false,defaultValue = "5")int pageSize, HttpServletRequest request){
        PageBean<OfficialProductEntity> pageBean=productService.listPage(null,currentPage,pageSize);
        pageBean.setUrl(request.getContextPath() + "/portal/home/product?title="+title);
        request.setAttribute("pageBean",pageBean);
        return "portal/home/product";
    }

    /**
     * 获取产品详情
     * @param productId 产品id
     * @param request
     * @return
     */
    @RequestMapping("/product/{productId}")
    public String productDetail(@PathVariable int productId, HttpServletRequest request){
        OfficialProductEntity productDetail= productService.getProductById(productId);
        request.setAttribute("productDetail",productDetail);
        return "portal/home/productDetail";
    }

    /**
     * 获取服务列表
     * @param title 产品标题
     * @param currentPage 页数
     * @param pageSize 一页大小
     * @param request
     * @return
     */
    @RequestMapping("/service")
    public String service(String title, @RequestParam(required = false,defaultValue = "1") int currentPage, @RequestParam(required = false,defaultValue = "5")int pageSize, HttpServletRequest request){
        PageBean<OfficialServiceEntity> pageBean=serviceService.listPage(null,currentPage,pageSize);
        pageBean.setUrl(request.getContextPath() + "/portal/home/service?title="+title);
        request.setAttribute("pageBean",pageBean);
        return "portal/home/service";
    }

    /**
     * 获取新闻详情
     * @param serviceId 服务id
     * @param request
     * @return
     */
    @RequestMapping("/service/{serviceId}")
    public String serviceDetail(@PathVariable int serviceId, HttpServletRequest request){
        OfficialServiceEntity serviceDetail= serviceService.getServiceById(serviceId);
        request.setAttribute("serviceDetail",serviceDetail);
        return "portal/home/serviceDetail";
    }

    /**
     * 获取新闻列表
     * @param title 新闻标题
     * @param currentPage 页数
     * @param pageSize 一页大小
     * @param request
     * @return
     */
    @RequestMapping("/news")
    public String news(String title, @RequestParam(required = false,defaultValue = "1") int currentPage, @RequestParam(required = false,defaultValue = "5")int pageSize, HttpServletRequest request){
        PageBean<OfficialNewsEntity> pageBean=newsService.listPage(null,currentPage,pageSize);
        pageBean.setUrl(request.getContextPath() + "/portal/home/news?title="+title);
        request.setAttribute("pageBean",pageBean);
        return "portal/home/news";
    }

    /**
     * 获取新闻详情
     * @param newsId 新闻id
     * @param request
     * @return
     */
    @RequestMapping("/news/{newsId}")
    public String news(@PathVariable int newsId, HttpServletRequest request){
        OfficialNewsEntity newsDetail= newsService.getNewsById(newsId);
        request.setAttribute("newsDetail",newsDetail);
        return "portal/home/newsDetail";
    }

    @RequestMapping("/function")
    public String function(){
        return "portal/home/function";
    }

    @RequestMapping("/about")
    public String about(){
        return "portal/home/about";
    }

    @RequestMapping("/contact")
    public String contact(){
        return "portal/home/contact";
    }

    @RequestMapping("/map")
    public String map(){
        return "portal/home/map";
    }

    @RequestMapping("/feedback")
    public String feedback(){
        return "portal/home/feedback";
    }

    @RequestMapping("/message")
    public String message(){
        return "portal/home/message";
    }

    @RequestMapping("/license")
    public String license(){
        return "portal/home/license";
    }
}
