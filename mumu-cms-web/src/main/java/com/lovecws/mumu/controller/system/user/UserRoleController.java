package com.lovecws.mumu.controller.system.user;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.tree.ZTreeBean;
import com.lovecws.mumu.shiro.realm.UserRealm;
import com.lovecws.mumu.system.entity.SysRole;
import com.lovecws.mumu.system.service.SysRoleService;
import com.lovecws.mumu.system.service.SysUserRoleService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/system/user")
public class UserRoleController {

	private static final Logger log=Logger.getLogger(UserRoleController.class);
	@Autowired
	private SysUserRoleService userRoleService;
	@Autowired
	private SysRoleService roleService;
	@Autowired
	private UserRealm userRealm;
	
	/**
	 * 用户分配角色
	 * @return
	 */
	@RequiresPermissions("system:user:allowRole")
	@RequestMapping(value={"/allowRole"},method=RequestMethod.GET)
	public String allowRole(String userId,HttpServletRequest request){
		request.setAttribute("userId", userId);
		return "system/user/allowRole";
	}
	
	/**
	 * 用户分配角色
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:allowRole")
	@RequestMapping(value={"/userRole/{userId}"},method=RequestMethod.GET)
	public ZTreeBean userRole(@PathVariable String userId){
		//获取所有的角色
		List<SysRole> allRoles = roleService.querySysRoleByCondition(null, null, null, PublicEnum.NORMAL.value());
		//获取当前用户的角色
		List<SysRole> selectedRoles = roleService.getSysRoleByUserId(userId, PublicEnum.NORMAL.value());
		List<ZTreeBean> beans=new ArrayList<ZTreeBean>();
		for (SysRole sysRole : allRoles) {
			boolean checked=false;
			for (SysRole selectedRole : selectedRoles) {
				if(selectedRole.getRoleId()==sysRole.getRoleId()){
					checked=true;
					break;
				}
			}
			beans.add(new ZTreeBean(String.valueOf(sysRole.getRoleId()), "topRole", sysRole.getRoleName(),true,"",checked));
		}
		ZTreeBean bean=new ZTreeBean("topRole", "0", "角色树", true, "", "", false, beans);
		return bean;
	}
	
	/**
	 * 用户分配角色
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:allowRole")
	@RequestMapping(value={"/userRole"},method=RequestMethod.POST)
	public ResponseEntity saveUserRole(String userId,String roleIds){
		try {
			userRoleService.saveUserRole(userId, roleIds, SecurityUtils.getSubject().getPrincipal().toString());
			//删除当前登录的权限
			userRealm.clearPermissionCache(userId);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(200, "用户角色保存出现异常", null);
		}
		return new ResponseEntity(200, "用户角色保存成功", null);
	}
}
