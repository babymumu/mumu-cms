package com.lovecws.mumu.controller.system.user;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.shiro.entity.BaseRealm;
import com.lovecws.mumu.shiro.utils.PasswordHelper;
import com.lovecws.mumu.system.entity.SysUser;
import com.lovecws.mumu.system.service.SysUserService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/user")
public class UserController {

	private static final Logger log =Logger.getLogger(UserController.class);
	@Autowired
	private SysUserService userService;
	
	/**
	 * 用户列表
	 * @return
	 */
	@RequiresPermissions("system:user:view")
	@RequestMapping(value={"/list"},method=RequestMethod.GET)
	public String list(){
		return "system/user/list";
	}
	
	/**
	 * 分页获取用户数据
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:view")
	@RequestMapping(value={"/page"},method=RequestMethod.GET)
	public Map<String,Object> page(String userName,String userEmail,String userPhone,int beginIndex,int pageSize){
		PageBean<SysUser> pageBean = userService.listPage(userName,userEmail,userPhone, beginIndex / pageSize + 1, pageSize);
		Map<String, Object> page = new HashMap<String, Object>();
		page.put("total", pageBean.getTotalCount());
		page.put("rows", pageBean.getRecordList());
		return page;
	}
	
	/**
	 * 跳转到添加用户页面
	 * @return
	 */
	@RequiresPermissions("system:user:add")
	@RequestMapping(value={"/add"},method=RequestMethod.GET)
	public String addUser(){
		return "system/user/add";
	}
	
	/**
	 * 保存用户
	 * @param user 用户实体
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:add")
	@RequestMapping(value={"/add"},method=RequestMethod.POST)
	public ResponseEntity saveUser(SysUser user){
		List<SysUser> users = userService.querySysUserByCondition(user.getUserName(), null, user.getEmail(), user.getPhone(), null);
		if(users!=null&&users.size()>0){
			String errorMsg="用户名称、用户手机号码、用户邮箱不可重复";
			for (SysUser sysUser : users) {
				if(sysUser.getUserName().equals(user.getUserName())){
					errorMsg="用户名称不可重复";
					break;
				}else if(sysUser.getPhone().equals(user.getPhone())){
					errorMsg="用户手机号码不可重复";
					break;
				}else if(sysUser.getEmail().equals(user.getEmail())){
					errorMsg="用户邮箱不可重复";
					break;
				}
			}
			return new ResponseEntity(500, errorMsg, null);
		}
		//加密密码
		BaseRealm baseRealm = PasswordHelper.encryptPassword(new BaseRealm(user.getUserName(), user.getPassword()));
		user.setPassword(baseRealm.getPassword());
		user.setSalt(baseRealm.getSalt());
		try {
			user.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
			userService.addUser(user);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "保存用户出现异常", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 跳转到查看用户详情页面
	 * @return
	 */
	@RequiresPermissions("system:user:view")
	@RequestMapping(value={"/view/{userId}"},method=RequestMethod.GET)
	public String view(@PathVariable String userId,HttpServletRequest request){
		SysUser user = userService.getSysUserById(userId);
		request.setAttribute("user", user);
		return "system/user/view";
	}
	
	/**
	 * 跳转到编辑用户页面
	 * @return
	 */
	@RequiresPermissions("system:user:edit")
	@RequestMapping(value={"/edit/{userId}"},method=RequestMethod.GET)
	public String edit(@PathVariable String userId,HttpServletRequest request){
		SysUser user = userService.getSysUserById(userId);
		request.setAttribute("user", user);
		return "system/user/edit";
	}
	
	/**
	 * 更新用户信息
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:edit")
	@RequestMapping(value={"/edit"},method=RequestMethod.PUT)
	public ResponseEntity updateUser(SysUser user){
		List<SysUser> users = userService.querySysUserByCondition(user.getUserName(), null, user.getEmail(), user.getPhone(), null);
		if(users!=null&&users.size()>0&&user.getUserId()!=users.get(0).getUserId()){
			String errorMsg="用户名称、用户手机号码、用户邮箱不可重复";
			for (SysUser sysUser : users) {
				if(sysUser.getUserName().equals(user.getUserName())){
					errorMsg="用户名称不可重复";
					break;
				}else if(sysUser.getPhone().equals(user.getPhone())){
					errorMsg="用户手机号码不可重复";
					break;
				}else if(sysUser.getEmail().equals(user.getEmail())){
					errorMsg="用户邮箱不可重复";
					break;
				}
			}
			return new ResponseEntity(500, errorMsg, null);
		}
		try {
			user.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
			userService.updateById(user);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "更新用户出现异常", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 跳转到编辑用户页面
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:delete")
	@RequestMapping(value={"/delete/{userId}"},method=RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable String userId){
		try {
			userService.deleteById(userId);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "删除用户出现异常", null);
		}
		return new ResponseEntity();
	}
	
	/**
	 * 重置密码
	 * @param userId 用户id
	 * @param userName 用户名称
	 * @param password 用户密码
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("system:user:resetPassword")
	@RequestMapping(value={"/resetPwd"},method=RequestMethod.PUT)
	public ResponseEntity resetPwd(String userId,String userName,String password){
		try {
			SysUser user=new SysUser();
			user.setUserId(Integer.parseInt(userId));
			user.setUserName(userName);
			//加密密码
			BaseRealm baseRealm = PasswordHelper.encryptPassword(new BaseRealm(userName, password));
			user.setPassword(baseRealm.getPassword());
			user.setSalt(baseRealm.getSalt());
			user.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
			userService.updateById(user);
		} catch (Exception e) {
			log.error(e);
			return new ResponseEntity(500, "用户重置密码出现异常", null);
		}
		return new ResponseEntity(200,"用戶重置密码成功",null);
	}
	
	/**
	 * 注册统计
	 * @return
	 */
	@RequiresPermissions("system:user:statistics")
	@RequestMapping(value={"/regStatistics"},method=RequestMethod.GET)
	public String regStatistics(){
		System.out.println("regStatistics");
		return "system/user/regStatistics";
	}
	
	/**
	 * 地区分布统计
	 * @return
	 */
	@RequiresPermissions("system:user:statistics")
	@RequestMapping(value={"/areaStatistics"},method=RequestMethod.GET)
	public String areaStatistics(){
		System.out.println("areaStatistics");
		return "system/user/areaStatistics";
	}
	/**
	 * 地区分布统计
	 * @return
	 */
	@RequiresPermissions("system:user:statistics")
	@RequestMapping(value={"/hareaStatistics"},method=RequestMethod.GET)
	public String hareaStatistics(){
		System.out.println("hareaStatistics");
		return "system/user/hareaStatistics";
	}
	
	/**
	 * 访问统计
	 * @return
	 */
	@RequiresPermissions("system:user:statistics")
	@RequestMapping(value={"/accessStatistics"},method=RequestMethod.GET)
	public String accessStatistics(){
		System.out.println("accessStatistics");
		return "system/user/accessStatistics";
	}
	
	/**
	 * 性别统计
	 * @return
	 */
	@RequiresPermissions("system:user:statistics")
	@RequestMapping(value={"/sexStatistics"},method=RequestMethod.GET)
	public String sexStatistics(){
		System.out.println("sexStatistics");
		return "system/user/sexStatistics";
	}
	
	/**
	 * 年龄段统计
	 * @return
	 */
	@RequiresPermissions("system:user:statistics")
	@RequestMapping(value={"/ageStatistics"},method=RequestMethod.GET)
	public String ageStatistics(){
		System.out.println("ageStatistics");
		return "system/user/ageStatistics";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
