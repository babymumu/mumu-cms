package com.lovecws.mumu.controller.portal.account;

import com.lovecws.mumu.common.core.response.HttpCode;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.shiro.entity.BaseRealm;
import com.lovecws.mumu.shiro.utils.PasswordHelper;
import com.lovecws.mumu.system.entity.SysUser;
import com.lovecws.mumu.system.service.SysUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2017/3/8.
 */
@Controller
@RequestMapping("/portal/account")
public class UserMessageController {

    private static final Logger log=Logger.getLogger(UserMessageController.class);

    @Autowired
    private SysUserService userService;

    /**
     * 未登陆用户修改密码
     * @param userPhone 用户手机号码
     * @param userEmail 用户邮件
     * @param password 修改后的密码
     * @return
     */
    @ResponseBody
    @RequestMapping(value={"/changePwd"},method= RequestMethod.PUT)
    public ResponseEntity changePassword(String userPhone, String userEmail, String password){
        SysUser sysUser=userService.getUserByCondition(null,userPhone,userEmail);
        if(sysUser==null){
            return new ResponseEntity(HttpCode.PARAMETER_ERROR,"用户信息泄露,请重新找回密码");
        }
        try {
            SysUser user=new SysUser();
            user.setUserId(sysUser.getUserId());
            user.setUserName(sysUser.getUserName());
            //加密密码
            BaseRealm baseRealm = PasswordHelper.encryptPassword(new BaseRealm(user.getUserName(), password));
            user.setPassword(baseRealm.getPassword());
            user.setSalt(baseRealm.getSalt());
            userService.updateById(user);
        } catch (Exception e) {
            log.error(e);
            return new ResponseEntity(HttpCode.SERVER_ERROR, "用户重置密码出现异常");
        }
        return new ResponseEntity(HttpCode.OK,"用戶重置密码成功");
    }
}
