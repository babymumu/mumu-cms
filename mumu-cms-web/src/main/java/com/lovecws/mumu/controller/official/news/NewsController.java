package com.lovecws.mumu.controller.official.news;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.response.ResponseEntity;
import com.lovecws.mumu.common.core.utils.StringUtil;
import com.lovecws.mumu.official.entity.OfficialNewsCategoryEntity;
import com.lovecws.mumu.official.entity.OfficialNewsEntity;
import com.lovecws.mumu.official.service.OfficialNewsCategoryService;
import com.lovecws.mumu.official.service.OfficialNewsService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ganliang on 2017/3/13.
 * 新闻集合控制器
 */
@Controller
@RequestMapping("/official/news")
public class NewsController {

    @Autowired
    private OfficialNewsService newsService;
    @Autowired
    private OfficialNewsCategoryService newsCategoryService;

    @RequiresPermissions(value = {"official:news:view","official:news:categoryView"},logical= Logical.OR)
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(){
        return "official/news/list";
    }

    /**
     * 新闻分类
     * @return
     */
    @RequiresPermissions("official:news:view")
    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String news(){
        return "official/news/news";
    }

    /**
     *分页获取新闻列表
     * @param searchContent 搜索内容(标题或者分类id)
     * @param beginIndex 分页开始索引
     * @param pageSize 分页结束索引
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:view")
    @RequestMapping(value = "/newsPage", method = RequestMethod.GET)
    public Map<String,Object> newsPage(String searchContent, int beginIndex, int pageSize){
        PageBean<OfficialNewsEntity> pageBean=newsService.listPage(searchContent,beginIndex/pageSize+1,pageSize);
        Map<String,Object> page=new HashMap<String,Object>();
        page.put("total", pageBean.getTotalCount());
        page.put("rows", pageBean.getRecordList());
        return page;
    }

    /**
     * 跳转到添加新闻列表页面
     * @return
     */
    @RequiresPermissions("official:news:add")
    @RequestMapping(value = "/newsAdd", method = RequestMethod.GET)
    public String newsAdd(HttpServletRequest request){
        List<OfficialNewsCategoryEntity> newsCategorys=newsCategoryService.getNewsCategoryByCondition(null);
        request.setAttribute("newsCategorys",newsCategorys);
        return "official/news/newsAdd";
    }

    /**
     * 添加新闻
     * @param title 新闻标题
     * @param categoryId 新闻分类
     * @param logo 新闻logo
     * @param content 新闻内容
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:add")
    @RequestMapping(value = "/newsAdd", method = RequestMethod.POST)
    public ResponseEntity saveNewss(String title,int categoryId,String logo,String content){
        //校验参数
        if(StringUtil.isEmpty(title)){
            return new ResponseEntity(400,"新闻标题不能为空!",null);
        }
        if(StringUtil.isEmpty(logo)){
            return new ResponseEntity(400,"新闻logo图标不能为空!",null);
        }
        OfficialNewsEntity news=new OfficialNewsEntity();
        news.setCreateTime(new Date());
        news.setCreator(SecurityUtils.getSubject().getPrincipal().toString());
        news.setStatus(PublicEnum.NORMAL.value());
        news.setTitle(title);
        news.setCategoryId(categoryId);
        news.setLogo(logo);
        news.setContent(content);
        newsService.addNews(news);
        return new ResponseEntity(200,"保存新闻操作成功!",null);
    }

    /**
     * 查看新闻分类详情
     * @param newsId 新闻id
     * @param request
     * @return
     */
    @RequiresPermissions("official:news:view")
    @RequestMapping(value = "/newsView/{newsId}", method = RequestMethod.GET)
    public String newsView(@PathVariable int newsId, HttpServletRequest request){
        OfficialNewsEntity news=newsService.getNewsById(newsId);
        request.setAttribute("news",news);
        return "official/news/newsView";
    }

    /**
     * 跳转到编辑新闻页面
     * @param newsId 新闻id
     * @param request
     * @return
     */
    @RequiresPermissions("official:news:edit")
    @RequestMapping(value = "/newsEdit/{newsId}", method = RequestMethod.GET)
    public String newsEdit(@PathVariable int newsId, HttpServletRequest request){
        OfficialNewsEntity news=newsService.getNewsById(newsId);
        request.setAttribute("news",news);

        List<OfficialNewsCategoryEntity> newsCategorys=newsCategoryService.getNewsCategoryByCondition(null);
        request.setAttribute("newsCategorys",newsCategorys);
        return "official/news/newsEdit";
    }

    /**
     * 更新新闻
     * @param newsId
     * @param title
     * @param categoryId
     * @param logo
     * @param content
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:edit")
    @RequestMapping(value = "/newsEdit", method = RequestMethod.POST)
    public ResponseEntity newsEdit(int newsId,String title,int categoryId,String logo,String content){
        OfficialNewsEntity newsEntity=newsService.getNewsById(newsId);
        if(newsEntity==null){
            return new ResponseEntity(400,"新闻不存在",null);
        }
        OfficialNewsEntity news=new OfficialNewsEntity();
        news.setId(newsId);
        news.setEditor(SecurityUtils.getSubject().getPrincipal().toString());
        news.setEditTime(new Date());
        news.setTitle(title);
        news.setCategoryId(categoryId);
        news.setLogo(logo);
        news.setContent(content);
        newsService.updateNews(news);
        return new ResponseEntity(200,"保存新闻操作成功!",null);
    }

    /**
     * 删除新闻
     * @param newsId 新闻id
     * @return
     */
    @ResponseBody
    @RequiresPermissions("official:news:delete")
    @RequestMapping(value = "/newsDelete/{newsId}", method = RequestMethod.DELETE)
    public ResponseEntity newsDelete(@PathVariable int newsId){
        OfficialNewsEntity news=newsService.getNewsById(newsId);
        if(news==null){
            return new ResponseEntity(400,"新闻不存在",null);
        }
        newsService.deleteNewsById(newsId);
        return new ResponseEntity(200,"删除新闻操作成功",null);
    }

    /**
     * 获取只是显示的新闻列表
     * @param categoryId 新闻分类id
     * @param request
     * @return
     */
    @RequiresPermissions("official:news:view")
    @RequestMapping(value = "/newsList/{categoryId}", method = RequestMethod.GET)
    public String newsList(@PathVariable int categoryId,HttpServletRequest request){
        request.setAttribute("cid",categoryId);
        return "official/news/newsList";
    }
}
