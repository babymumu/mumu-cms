$(function() {
	var slider = new SliderUnlock("#slider", {
		successLabelTip : "验证成功"
	}, function() {
		$("#verifyCoding").val("success");
	});
	slider.init();
})	
function changeCaptchaCode(){
	var imgSrc=$("#captchaCodeImg")[0];
	imgSrc.src=imgSrc.src+"?date="+new Date().getTime();
}

//发送找回密码邮件
function sendVerifyRegEmail() {
	//验证邮箱
	var email=$("input[name='email']").val();
	if(email==null||email==''){
		layer.alert("请输入有效的邮箱!");
		return false;
	}
	//验证验证码
	var captchaCode=$("input[name='captchaCode']").val();
	if(captchaCode==null||captchaCode==''){
		layer.alert("请输入验证码!");
		return false;
	}
	//滑动验证
	var verifyCoding=$("#verifyCoding").val();
	if(verifyCoding!='success'){
		layer.alert("请滑动验证!");
		return false;
	}
	//发送邮件
	$.post("../../common/email/sendRegEmail",{email:email,captchaCode:captchaCode},function(response){
		if(response.code==200){
			layer.msg('邮箱发送成功,请登录邮箱获取邮箱验证码。',{icon:6,time:2000}, function(){
				$("input[name='regToken']").val(response.data);
				$("#regEmailForm").submit();
			});
		}else{
			layer.alert(response.data);
		}
	});
	return false;
}
