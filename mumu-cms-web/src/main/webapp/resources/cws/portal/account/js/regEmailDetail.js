//验证用户注册信息
function verifyEmailRegister(){
	//用户邮箱
	var email=$("input[name='email']").val();
	if(email==null||email==''){
		layer.alert("注册邮箱不能为空!");
		return false;
	}
	//验证用户名称
	var userName=$("input[name='userName']").val();
	if(userName==null||userName==''){
		layer.alert("注册用户名称不能为空!");
		return false;
	}
	if(userName.length<6){
		layer.alert("注册用户名称长度小于6!");
		return false;
	}
	//验证用户密码
	var password=$("input[name='password']").val();
	var repassword=$("input[name='repassword']").val();
	if(password==null||password==''||repassword==null||repassword==''){
		layer.alert("注册密码不能为空!");
		return false;
	}
	if(password.length<6||repassword.length<6){
		layer.alert("注册密码长度不能小于6!");
		return false;
	}
	//验证两次输入密码是否一致
    if(password!=repassword){
		layer.alert("注册密码不一致!");
		return false;
	}
    //验证码
	var regToken=$("input[name='regToken']").val();
	var captchaCode=$("input[name='captchaCode']").val();
	if(captchaCode==null||captchaCode==''){
		layer.alert("验证码不能为空!");
		return false;
	}
	if(regToken!=captchaCode){
		layer.alert("验证码不正确!");
		return false;
	}
	//注册用户
	$.post("../../portal/account/verifyReg",{userEmail:email,userName:userName,password:password,captchaCode:null},function(response){
		if(response.code==200){
			$("#regEmailDetailForm").submit();
		}else{
			layer.alert(response.data,{icon:1});
		}
	});
	return false;
}
function setEmailAddress(email){
	if(email==null){
		return;
	}
    var emailArrays=email.split("@");
	if(emailArrays.length<2){
		return;
	}
	var emailSufix=emailArrays[1];
	var emailAddress=null;
	switch (emailSufix){
		case "163.com":
			emailAddress="http://mail.163.com/";
			break;
		case "gmail.com":
			emailAddress="https://mail.google.com";
			break;
		case "qq.com":
			emailAddress="https://mail.qq.com/";
			break;
		default:
			emailAddress="https://www.baidu.com/s?wd="+emailSufix;
			break;
	}
	$("#emailAddress").attr("href",emailAddress);
}