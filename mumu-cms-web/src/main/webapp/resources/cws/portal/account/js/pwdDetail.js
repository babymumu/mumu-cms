//修改密码
function changePassword(){

    //手机号码 邮箱
    var userPhone=$("input[name='userPhone']").val();
    var userEmail=$("input[name='userEmail']").val();

    //用户输入的密码 和再次密码
    var password=$("input[name='password']").val();
    var repassword=$("input[name='repassword']").val();
    if(password==null||password==''||repassword==null||repassword==''){
        layer.alert("输入的密码不能为空!");
        return false;
    }
    if(repassword!=password){
        layer.alert("两次输入的密码不一致!");
        return false;
    }
    //修改用户密码
    $.post("../../portal/account/changePwd",{userPhone:userPhone,userEmail:userEmail,password:password,_method:'put'},function(response){
        if(response.code==200){
            //密码修改成功
            $("#pwdDetailForm").submit();
        }else{
            layer.alert(response.data);
        }
    });
    return false;
}
