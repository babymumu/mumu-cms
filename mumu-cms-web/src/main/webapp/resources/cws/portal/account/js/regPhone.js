$(function() {
	var slider = new SliderUnlock("#slider", {
		successLabelTip : "验证成功"
	}, function() {
		$("#verifyCoding").val("success");
	});
	slider.init();
})	
function sendMessage(){
	alert("未实现");
}
//发送短信验证码
function sendMessage(){
	//验证手机号码不能为空
	var userPhone=$("input[name='userPhone']").val();
	if(userPhone==null||userPhone==''){
		layer.alert("请输入手机号码!");
		return false;
	}
	//发送短信
	$.post("../../common/sms/send",{userPhone:userPhone},function(response){
		if(response.code==200){
			$("#smsCode").val(response.data);

			//短信验证码发送后 进入倒计时
			$("#sendMessageButton").attr("value",60);
			$("#sendMessageButton").attr("disabled",true);
			//定时器 每一秒调用一次
			var interval=setInterval(function(){
				sendMessageInterval();
			},1000);
			//定时任务
			function sendMessageInterval() {
				var val=$("#sendMessageButton").attr("value");
				//当倒计时为0时 结束倒计时 回复按钮原来面貌
				if(val<=0){
					clearInterval(interval);
					$("#sendMessageButton").attr("value","发送");
					$("#sendMessageButton").attr("disabled",false);
				}else{
					$("#sendMessageButton").attr("value",val-1);
				}
			}
		}else{
			layer.alert(response.data);
		}
	});
}
//验证用户手机号码 和短信验证码
function verifyUserPhoneAndSmsMessage(){
	//验证手机号码不能为空
	var userPhone=$("input[name='userPhone']").val();
	if(userPhone==null||userPhone==''){
		layer.alert("请输入手机号码!");
		return false;
	}
	//获取短信验证码
	var smsCode=$("#smsCode").val(),verifyCode=$("input[name='verifyCode']").val();
	if(smsCode==null||smsCode==''||verifyCode==null||verifyCode==''){
		layer.alert("请获取短信验证码!");
		return false;
	}
	//获取滑动条验证
	var verifyCoding=$("#verifyCoding").val();
	if(verifyCoding!='success'){
		layer.alert("请滑动验证条!");
		return false;
	}
	//请求服务器验证用户信息
	$.post("../../common/sms/verifyReg",{userPhone:userPhone,smsCode:smsCode,verifyCode:verifyCode},function(response){
		if(response.code==200){
			$("#regPhoneForm").submit();
		}else{
			layer.alert(response.data);
		}
	});
	return false;
}