function changeCaptchaCode(){
	var imgSrc=$("#captchaCodeImg")[0];
	imgSrc.src=imgSrc.src+"?date="+new Date().getTime();
}
//验证用户注册信息
function verifyRegister(){
	//用户手机号码
	var userPhone=$("input[name='userPhone']").val();
	if(userPhone==null||userPhone==''){
		layer.alert("注册用户手机号码不能为空!");
		return false;
	}
	//验证用户名称
	var userName=$("input[name='userName']").val();
	if(userName==null||userName==''){
		layer.alert("注册用户名称不能为空!");
		return false;
	}
	if(userName.length<6){
		layer.alert("注册用户名称长度小于6!");
		return false;
	}
	//验证用户密码
	var password=$("input[name='password']").val();
	var repassword=$("input[name='repassword']").val();
	if(password==null||password==''||repassword==null||repassword==''){
		layer.alert("注册密码不能为空!");
		return false;
	}
	//验证两次输入密码是否一致
    if(password!=repassword){
		layer.alert("注册密码不一致!");
		return false;
	}
    //验证码
	var captchaCode=$("input[name='captchaCode']").val();
	if(captchaCode==null||captchaCode==''){
		layer.alert("验证码不能为空!");
		return false;
	}
	//注册用户
	$.post("../../portal/account/verifyReg",{userPhone:userPhone,userName:userName,password:password,captchaCode:captchaCode},function(response){
		if(response.code==200){
			$("#regPhoneDetailForm").submit();
		}else{
			layer.alert(response.data);
		}
	});
	return false;
}