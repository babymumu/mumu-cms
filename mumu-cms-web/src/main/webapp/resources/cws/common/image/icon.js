/**
 * 图标
 */
$(function(){
	var icon="";
	$(".container-fluid .bs-glyphicons li").click(function(){
		$(".container-fluid .bs-glyphicons li").removeClass("highlight");
		$(this).addClass("highlight");
		var span=$(this).children()[0];
		icon=$(span).attr("class");
	});
	//保存图标
	$(".saveIconButton").click(function(){
		if(parent.window!=self){
			parent.window.$(".menuIcon").val(icon);
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭   
		}
	});
});