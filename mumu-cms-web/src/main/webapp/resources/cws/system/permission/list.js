/**
 * 菜单列表
 */
layui.use(['laypage', 'layer','element']);
//添加权限
function addPermission(){
	top.window.layer.open({
		 type: 2, 
		 title :'添加权限',
		 maxmin: true,
		 content: ['../../system/permission/add','yes'],
		 area: ['800px', '600px'],
		 end:function(){
			 queryPermission();
		 }
	});
}
//初始化表格
function initTable() {
	var $table = $('#permissionTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '权限ID',
                    	field: 'permissionId',
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    }, {
                        title: '权限内码',
                        field:'permissionCode',
                        width:150,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '权限名称',
                        field: 'permissionName',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '权限值',
                        field:'permission',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '权限路径',
                        field:'permissionPath',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
function operateFormatter(value, row, index) {
	/*var buttons=[];
	buttons.push('<button type="button" class="btn btn-default btn-sm permissionDetail"><span class="glyphicon glyphicon-eye-open"></span>查看</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm editPermission"><span class="glyphicon glyphicon-edit"></span>编辑</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm deletePermission"><span class="glyphicon glyphicon-trash"></span>删除</button>');*/
    return permissionButtons.join('');
}
//表格点击事件
var operateEvents = {
        //权限详情
		'click .permissionDetail': function (e, value, row, index) {
			top.window.layer.open({
				 type: 2, 
				 title :'查看权限详情',
				 maxmin: true,
				 content: ['../../system/permission/view?permissionId='+row.permissionId,'yes'],
				 area: ['800px', '600px']
			});
        },
        //编辑权限
        'click .editPermission': function (e, value, row, index) {
        	top.window.layer.open({
				 type: 2, 
				 title :'编辑权限',
				 maxmin: true,
				 content: ['../../system/permission/edit?permissionId='+row.permissionId,'yes'],
				 area: ['800px', '600px'],
				 end:function(){
					 queryPermission();
				 }
			});
        },
        //删除权限
        'click .deletePermission': function (e, value, row, index) {
        	layer.confirm('你确定要删除权限【'+row.permission+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/permission/delete",{permissionId:row.permissionId,_method:'delete'},function(data){
        			top.layer.msg(data.msg,{
    					icon: 1,
    					skin: 'demo-class',
    					title :'信息',
    					time:500,
    				    area: ['300px', '160px'],
    				    offset: 'rb',
    				    end :function(){
    				    	if(data.code==200){
    				    		layer.close(index);   
    				    		//刷新表格数据
    				    		queryPermission();
    						}
    				    }
    				});
        		});
        	});
        }
};
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	menuId:$('#subMenuSelect').children('option:selected').val(),
    	permissionName:$("input[name='permissionName']").val()
    };
}
//表单查询
function queryPermission(){
	var $table = $('#permissionTable');    
	$table.bootstrapTable("refresh");
}
$(function(){
	initTable();
	msg('权限信息加载完毕');
});