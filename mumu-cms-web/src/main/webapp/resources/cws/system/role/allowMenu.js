/**
 * 角色分配菜单
 */
//ztree基本配置
var zTreeObj;
var setting = {
    view: {  
           showLine: false,  
           dblClickExpand: true  
          },  
    callback:{  
               
             },  
	check: {
			enable: true
		   },
	data: {
		simpleData: {
			enable: true,
			idKey: "id",  
            pIdKey: "pId",  
            rootPId: 0
		}
	}
};   
var ztree=$.fn.zTree;
//保存角色菜单
function saveRoleMenu(roleId){
	var menuIds=[];
	var nodes=zTreeObj.getCheckedNodes();
	for(var i=0;i<nodes.length;i++){
		var menuId=nodes[i].id;
		if(menuId!='topMenu'){
			menuIds.push(nodes[i].id);
		}
	}
	console.log(menuIds.join(','));
	//保存用户分配的角色
	$.post('../../system/role/roleMenu',{roleId:roleId,menuIds:menuIds.join(',')},function(data){
		top.layer.msg(data.msg,{
			icon: 1,
			skin: 'demo-class',
			title :'信息',
			time:2000,
		    area: ['300px', '160px'],
		    offset: 'rb',
		    end :function(){
		    	if(data.code==200){
					parent.layer.closeAll();	
				}
		    }
		});
	});
}
$(function(){
	var roleId=$("input[name='roleId']").val();
    $.get("../../system/role/roleMenu/"+roleId,function(data){
          zTreeObj = ztree.init($("#roleAllowMenuTree"), setting, data);
    });
});