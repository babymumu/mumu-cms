/**
 * 角色分配权限
 */
//ztree基本配置
var zTreeObj;
var setting = {
    view: {  
           showLine: false,  
           dblClickExpand: true  
          },  
    callback:{  
               
             },  
	check: {
			enable: true
		   },
	data: {
		simpleData: {
			enable: true,
			idKey: "id",  
            pIdKey: "pId",  
            rootPId: 0
		}
	}
};   
var ztree=$.fn.zTree;
//保存用户角色
function saveRolePermission(roleId){
	var permissionIds=[];
	var nodes=zTreeObj.getCheckedNodes();
	for(var i=0;i<nodes.length;i++){
		var permissionId=nodes[i].id;
		if(permissionId.indexOf('p')==0){
			permissionIds.push(permissionId.substring(1));
		}
	}
	//保存用户分配的角色
	$.post('../../system/role/rolePermission',{roleId:roleId,permissionIds:permissionIds.join(',')},function(data){
		if(data.code==200){
			top.layer.msg(data.msg,{
				icon: 1,
				skin: 'demo-class',
				title :'信息',
				time:2000,
			    area: ['300px', '160px'],
			    offset: 'rb',
			    end :function(){
			    	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭   	
			    }
			});
		}else{
			layer.alert(data.msg);
		}
	});
}
$(function(){
	var roleId=$("input[name='roleId']").val();
    $.get("../../system/role/rolePermission/"+roleId,function(data){
          zTreeObj = ztree.init($("#roleAllowPermissionTree"), setting, data);
    });
});