/**
 * 添加角色
 */
//表单验证
$(function(){
	layui.use(['form', 'layedit', 'laydate'], function(){
		var form = layui.form(),layer = layui.layer,layedit = layui.layedit,laydate = layui.laydate;
		//角色描述编辑器
		var roleDescIndex=layedit.build('roleDesc',{
			tool: [
				'strong' //加粗
				,'italic' //斜体
				,'underline' //下划线
				,'|' //分割线
				,'left' //左对齐
				,'center' //居中对齐
				,'right' //右对齐
			]
		}); //建立编辑器
		//监听提交
		form.on('submit(addRoleForm)', function(data){
			var remark=layedit.getContent(roleDescIndex);
    		data.field.remark=remark;
    		data.field._method='put';
			$.post("../../system/role/edit",data.field,function(data){
				if(data.code==200){
					//保存成功 关闭当前页
					top.layer.msg(data.msg,{
						icon: 1,
						skin: 'demo-class',
						title :'信息',
						time:500,
					    area: ['300px', '160px'],
					    offset: 'rb',
					    end :function(){
					    	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
							parent.layer.close(index); //再执行关闭   
					    }
					});
				}else{
					layer.alert(data.msg);
				}
			});
		    return false;
		});
	});
});