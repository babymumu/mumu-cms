/**
 * 角色列表
 */
$(function () {
	queryRole();
	msg('用户角色信息加载完毕');
});
function checkbox(record, rowIndex, colIndex, options) {
	return '<input type="checkbox" value="' + gridObj.getColumnValue(rowIndex, gridObj.getColumnModel(colIndex).index) + '"/>';
}
/*function operate(record, rowIndex, colIndex, options) {
	var buttons=[];
	buttons.push('<button type="button" class="btn btn-default btn-sm" onclick="roleDetail('+record.roleId+')"><span class="glyphicon glyphicon-eye-open"></span>查看</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm" onclick="editRole('+record.roleId+')"><span class="glyphicon glyphicon-edit"></span>编辑</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm" onclick="roleDelete('+record.roleId+','+record.roleName+')"><span class="glyphicon glyphicon-trash"></span>删除</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm" onclick="allowMenu('+record.roleId+')"><span class="glyphicon glyphicon-tree-deciduous"></span>分配菜单</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm" onclick="allowPermission('+record.roleId+')"><span class="glyphicon glyphicon-tree-deciduous"></span>分配权限</button>');
    return rolePermissionButtons.join('');
}*/
var gridObj;
//查询角色
function queryRole(){
	//gridObj.refreshPage();
	gridObj = $.fn.bsgrid.init('bsgridTable', {
    	url: '../../system/role/page',
    	pageSizeSelect: true,
        rowHoverColor: true,
        displayBlankRows: false,
        displayPagingToolbarOnlyMultiPages: true,
        pageSize: 10,
        otherParames:{
        	roleType:$('#roleTypeSelect').children('option:selected').val(),
        	roleName:$("input[name='roleName']").val()
        }
    });
}
//添加角色
function addRole(){
	top.window.layer.open({
		 type: 2, 
		 title :'添加角色',
		 maxmin: true,
		 content: ['../../system/role/add','yes'],
		 area: ['800px', '600px'],
		 end:function(){
			 queryRole();
		 }
	});
}
//角色详情
function roleDetail(roleId){
	top.window.layer.open({
		 type: 2, 
		 title :'角色详情',
		 maxmin: true,
		 content: ['../../system/role/view?roleId='+roleId,'yes'],
		 area: ['800px', '600px']
	});
}
//编辑角色
function editRole(roleId){
	top.window.layer.open({
		 type: 2, 
		 title :'编辑角色',
		 maxmin: true,
		 content: ['../../system/role/edit?roleId='+roleId,'yes'],
		 area: ['800px', '600px'],
		 end:function(){
			 queryRole();
		 }
	});
}
//角色删除
function roleDelete(roleId,roleName){
	layer.confirm('你确定要删除角色【'+roleName+'】吗?', {icon: 3, title:'提示'}, function(index){
		$.post("../../system/role/delete",{roleId:roleId,_method:'delete'},function(data){
			top.layer.msg(data.msg,{
				icon: 1,
				skin: 'demo-class',
				title :'信息',
				time:500,
			    area: ['300px', '160px'],
			    offset: 'rb',
			    end :function(){
			    	if(data.code==200){
			    		layer.close(index);   
			    		queryRole();
					}
			    }
			});
		});
	});
}
//角色分配菜单
function allowMenu(roleId){
	top.window.layer.open({
		 type: 2, 
		 title :'角色分配菜单',
		 maxmin: true,
		 content: ['../../system/role/allowMenu?roleId='+roleId,'yes'],
		 area: ['800px', '600px']
	});
}
//角色分配权限
function allowPermission(roleId){
	top.window.layer.open({
		 type: 2, 
		 title :'角色分配权限',
		 maxmin: true,
		 content: ['../../system/role/allowPermission?roleId='+roleId,'yes'],
		 area: ['800px', '600px']
	});
}