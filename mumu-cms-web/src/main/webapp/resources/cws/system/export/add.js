/**
 * 
 */
$(function(){
   $('#tablesSelect').change(function(){
	   var $table = $('#table');    
	   $table.bootstrapTable("refresh");
   });
});
function stateFormatter(value, row, index) {
    if(value==true||value=='true'){
    	return { checked: true}
    }
    return null;
}
//查询参数
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	tableName:$('#tablesSelect').children('option:selected').val()
    };
}
function saveExportModel(){
	var cnames=[],enames=[];
	var selections=$('#table').bootstrapTable('getAllSelections');
	for(var i=0,len=selections.length;i<len;i++){
	   var selection=selections[i];
	   enames.push(selection.fieldName);
	   if(!selection.remarks){
	     selection.remarks=" ";
	   }
	   cnames.push(selection.remarks);
	}
	var tableName=$('#tablesSelect').children('option:selected').val();
	if(enames.length==0){
		top.layer.alert("请选择导出字段");
		return false;
	}
	//保存
	$.post('../../system/export/add',{modelName:tableName,cnames:cnames.join(','),enames:enames.join(',')},function(data){
		//保存成功 关闭当前页
		top.layer.msg(data.msg,{
			icon: 1,
			skin: 'demo-class',
			title :'信息',
			time:500,
		    area: ['300px', '160px'],
		    offset: 'rb',
		    end :function(){
		    	if(data.code==200){
		    		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭   
				}
		    }
		});
	});
    return true;
}