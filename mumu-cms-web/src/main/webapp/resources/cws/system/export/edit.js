/**
 * 编辑导出模型
 */
function stateFormatter(value, row, index) {
	if (value == true || value == 'true') {
		return {
			checked : true
		}
	}
	return null;
}
//更新导出模型
function updateExportModel(modelId) {
	var cnames = [], enames = [];
	var selections = $('#table').bootstrapTable('getAllSelections');
	for (var i = 0, len = selections.length; i < len; i++) {
		var selection = selections[i];
		enames.push(selection.fieldName);
		if (!selection.remarks) {
			selection.remarks = " ";
		}
		cnames.push(selection.remarks);
	}
	if (enames.length == 0) {
		top.layer.alert("请选择导出字段");
		return false;
	}
	// 保存
	$.post('../../system/export/edit', {
		'_method':'put',
		modelId : modelId,
		cnames : cnames.join(','),
		enames : enames.join(',')
	}, function(data) {
		// 保存成功 关闭当前页
		top.layer.msg(data.msg, {
			icon : 1,
			skin : 'demo-class',
			title : '信息',
			time : 500,
			area : [ '300px', '160px' ],
			offset : 'rb',
			end : function() {
				if (data.code == 200) {
					var index = parent.layer.getFrameIndex(window.name); // 先得到当前iframe层的索引
					parent.layer.close(index); // 再执行关闭
				}
			}
		});
	});
	return true;
}