/**
 * 导出模型列表
 */
//查询表格
function queryExportModel(){
	var $table = $('#exportTable');    
	$table.bootstrapTable("refresh");
}
//添加导出模型
function addExportModel(){
	top.window.layer.open({
		 type: 2, 
		 title :'添加数据字典',
		 maxmin: true,
		 content: ['../../system/export/add','yes'],
		 area: ['800px', '600px'],
		 end :function(){
		     //刷新表格
			 queryExportModel();
		 }
	});
}
//初始化表格
function initTable() {
	var $table = $('#exportTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	field: 'modelId',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '模型名称',
                    	field: 'modelName',
                        align: 'center',
                        valign: 'middle'
                    }, {
                        title: '中文字段集合',
                        field:'cnames',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '英文字段集合',
                        field: 'enames',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
function operateFormatter(value, row, index) {
	/*return [
    	'<button type="button" class="btn btn-default btn-sm viewExportModel"><span class="glyphicon glyphicon-eye-open"></span>详情</button>',
    	'<button type="button" class="btn btn-default btn-sm editExportModel"><span class="glyphicon glyphicon-edit"></span>编辑</button>',
        '<button type="button" class="btn btn-default btn-sm deleteExportModel"><span class="glyphicon glyphicon-trash"></span>删除</button>'
    ].join('');*/
	return exportPermissionButtons.join("");
}
//表格点击事件
var operateEvents = {
		//模型详情
		'click .viewExportModel': function (e, value, row, index) {
			top.window.layer.open({
				 type: 2, 
				 title :'数据字典详情',
				 maxmin: true,
				 content: ['../../system/export/view?modelId='+row.modelId,'yes'],
				 area: ['800px', '600px']
			});
		},
		//编辑模型
		'click .editExportModel': function (e, value, row, index) {
			top.window.layer.open({
				 type: 2, 
				 title :'编辑数据字典',
				 maxmin: true,
				 content: ['../../system/export/edit?modelId='+row.modelId,'yes'],
				 area: ['800px', '600px'],
				 end :function(){
				     //刷新表格
					 queryExportModel();
				 }
			});
		},
        //删除模型
        'click .deleteExportModel': function (e, value, row, index) {
        	layer.confirm('你确定要删除导出模型【'+row.modelName+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/export/delete",{modelId:row.modelId,_method:'delete'},function(data){
        			top.layer.msg(data.msg,{
    					icon: 1,
    					skin: 'demo-class',
    					title :'信息',
    					time:500,
    				    area: ['300px', '160px'],
    				    offset: 'rb',
    				    end :function(){
    				    	if(data.code==200){
    				    		layer.close(index);   
    				    		//刷新表格数据
    				    		queryExportModel();
    						}
    				    }
    				});
        		});
        	});
        }
};
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	modelName:$("input[name='modelName']").val()
    };
}
$(function(){
	initTable();
	msg('导出设置加载完毕');
});