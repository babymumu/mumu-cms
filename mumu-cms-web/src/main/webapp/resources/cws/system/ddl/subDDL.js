/**
 * 菜单列表
 */
//添加数据字典
function addDDL(ddlCode,ddlName){
	var $table = $('#subDDLTable');
	$table.bootstrapTable("insertRow",{
		index:0,
		row:{
			state:false,
			ddlId:0,
			ddlCode:ddlCode,
			ddlName:ddlName,
			ddlKey:'',
			ddlValue:'',
			operate:''
		}
	});
}
//初始化表格
function initTable() {
	var $table = $('#subDDLTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        field: 'ddlId',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        field: 'ddlCode',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        field: 'ddlName',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '字典KEY',
                    	field: 'ddlKey',
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        editable:true
                    }, {
                        title: '字典值',
                        field:'ddlValue',
                        width:150,
                        align: 'center',
                        valign: 'middle',
                        editable:true
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
function operateFormatter(value, row, index) {
    /*return [
        '<button type="button" class="btn btn-default btn-sm deleteSubDDL"><span class="glyphicon glyphicon-trash"></span>删除</button>',
        '<button type="button" class="btn btn-default btn-sm saveSubDDL"><span class="glyphicon glyphicon-ok"></span>保存</button>'
    ].join('');*/
    return datasetPermissionButtons.join("");
}
//表格点击事件
var operateEvents = {
        //删除数据字典
        'click .deleteSubDDL': function (e, value, row, index) {
        	layer.confirm('你确定要删除数据字典【'+row.ddlValue+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/ddl/ddlSet",{ddlId:row.ddlId,_method:'delete'},function(data){
        			var $table = $('#subDDLTable');    
    				$table.bootstrapTable("refresh");
        			layer.close(index);  
        		});
        	});
        },
		//保存数据字典
		'click .saveSubDDL': function (e, value, row, index) {
			$.post('../../system/ddl/ddlSet',row,function(data){
				var $table = $('#subDDLTable');    
				$table.bootstrapTable("refresh");
			});
		}
};
$(function(){
	initTable();
});