/**
 * 菜单列表
 */
//查询表格
function queryDDL(){
	var $table = $('#ddlTable');    
	$table.bootstrapTable("refresh");
}
//添加数据字典
function addDDL(){
	top.window.layer.open({
		 type: 2, 
		 title :'添加数据字典',
		 maxmin: true,
		 content: ['../../system/ddl/add','yes'],
		 area: ['800px', '600px'],
		 end :function(){
		     //刷新表格
			 queryDDL();
		 }
	});
}
//初始化表格
function initTable() {
	var $table = $('#ddlTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '字典ID',
                        field: 'ddlId',
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '字典内码',
                    	field: 'ddlCode',
                        align: 'center',
                        valign: 'middle'
                    }, {
                        title: '字典名称',
                        field:'ddlName',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '描述',
                        field:'remark',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
function operateFormatter(value, row, index) {
    /*return [
    	'<button type="button" class="btn btn-default btn-sm viewDDL"><span class="glyphicon glyphicon-eye-open"></span>详情</button>',
    	'<button type="button" class="btn btn-default btn-sm editDDL"><span class="glyphicon glyphicon-edit"></span>编辑</button>',
        '<button type="button" class="btn btn-default btn-sm deleteDDL"><span class="glyphicon glyphicon-trash"></span>删除</button>',
    	'<button type="button" class="btn btn-default btn-sm subDDL"><span class="glyphicon glyphicon-th-list"></span>数据集</button>'
    ].join('');*/
    return ddlPermissionButtons.join('');
}
//表格点击事件
var operateEvents = {
		//查看数据字典详情
		'click .viewDDL': function (e, value, row, index) {
			top.window.layer.open({
				type: 2, 
				title :'数据字典详情',
				maxmin: true,
				content: ['../../system/ddl/view?ddlCode='+row.ddlCode,'yes'],
				area: ['800px', '600px']
			});
		},
		//编辑数据字典
		'click .editDDL': function (e, value, row, index) {
			top.window.layer.open({
				type: 2, 
				title :'编辑数据字典',
				maxmin: true,
				content: ['../../system/ddl/edit?ddlCode='+row.ddlCode,'yes'],
				area: ['800px', '600px'],
				end :function(){
				     //刷新表格
					 queryDDL();
				 }
			});
		},
		//查看子数据字典
		'click .subDDL': function (e, value, row, index) {
			top.window.layer.open({
				 type: 2, 
				 title :'数据字典集合',
				 maxmin: true,
				 content: ['../../system/ddl/subDDL?ddlCode='+row.ddlCode+"&ddlName="+row.ddlName,'yes'],
				 area: ['800px', '600px']
			});
		},
        //删除数据字典
        'click .deleteDDL': function (e, value, row, index) {
        	layer.confirm('你确定要删除数据字典【'+row.ddlName+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/ddl/delete",{ddlCode:row.ddlCode,_method:'delete'},function(data){
        			top.layer.msg(data.msg,{
    					icon: 1,
    					skin: 'demo-class',
    					title :'信息',
    					time:500,
    				    area: ['300px', '160px'],
    				    offset: 'rb',
    				    end :function(){
    				    	if(data.code==200){
    				    		layer.close(index);   
    				    		//刷新表格数据
    				    		queryDDL();
    						}
    				    }
    				});
        		});
        	});
        }
};
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	ddlName:$("input[name='ddlName']").val(),
    	ddlCode:$("input[name='ddlName']").val()
    };
}
$(function(){
	initTable();
	msg('数据字典加载完毕');
});