/**
 * 添加菜单
 */
$(function(){
	layui.use(['form', 'layedit', 'laydate'], function(){
		var form = layui.form(),layer = layui.layer,layedit = layui.layedit,laydate = layui.laydate;
		//角色描述编辑器
		var ddlDescIndex=layedit.build('ddlDesc'); //建立编辑器
		//自定义验证规则
		form.verify({
			//数据字典内码唯一性校验
			uniqueDDLCode:function(ddlCode){
			    if(ddlCode==null||ddlCode==''){
			       return "数据字典内码不能为空！";
			    }
			},
		    //数据字典名称唯一性校验
		    uniqueDDLName:function(ddlName){
			    if(ddlName==null||ddlName==''){
				   return "数据字典名称不能为空！";
			    }
		    }
		});
		//监听提交
		form.on('submit(addDDLForm)', function(data){
			//校验
		    $.get("../../system/ddl/exists?ddlCode="+data.field.ddlCode+"&ddlName="+data.field.ddlName,function(checkData){
		    	if(checkData.code==500){
		    		layer.alert(checkData.msg);
		    		return false;
		    	}else if(checkData.code==200){
		    		var remark=layedit.getText(ddlDescIndex);
					$.post("../../system/ddl/add",{ddlCode:data.field.ddlCode,ddlName:data.field.ddlName,remark:remark},function(data){
						//保存成功 关闭当前页
						top.layer.msg(data.msg,{
							icon: 1,
							skin: 'demo-class',
							title :'信息',
							time:500,
						    area: ['300px', '160px'],
						    offset: 'rb',
						    end :function(){
						    	if(data.code==200){
						    		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
									parent.layer.close(index); //再执行关闭   
								}
						    }
						});
					});
		    	}
		    });
		    return false;
		});
	});
});