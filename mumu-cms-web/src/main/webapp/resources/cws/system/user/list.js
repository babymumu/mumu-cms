/**
 * 用户列表
 */
layui.use(['laypage', 'layer','element']);
//添加用户
function addUser(){
	top.window.layer.open({
		 type: 2, 
		 title :'添加用户',
		 maxmin: true,
		 content: ['../../system/user/add','yes'],
		 area: ['800px', '600px'],
		 end:function(){
			 queryUser();
		 }
	});
}
//用户统计
function userStatistics(){
	top.layer.tab({
		 area: ['900px', '600px'],
		 maxmin: true,
		 shift:3,
		 tab: [{
		   title: '注册统计', 
		   content: '<iframe scrolling="yes" frameborder="0" src="../../system/user/regStatistics" style="width: 100%; height: 99.5%;"></iframe>'
		 }, {
		   title: '地区分布', 
		   content: '<iframe scrolling="yes" frameborder="0" src="../../system/user/hareaStatistics" style="width: 100%; height: 99.5%;"></iframe>'
		 }, {
		   title: '访问统计', 
		   content: '<iframe scrolling="yes" frameborder="0" src="../../system/user/accessStatistics" style="width: 100%; height: 99.5%;"></iframe>'
		 }, {
			   title: '性别统计', 
			   content: '<iframe scrolling="yes" frameborder="0" src="../../system/user/sexStatistics" style="width: 100%; height: 99.5%;"></iframe>'
		  }, {
			   title: '年龄统计', 
			   content: '<iframe scrolling="yes" frameborder="0" src="../../system/user/ageStatistics" style="width: 100%; height: 99.5%;"></iframe>'
		  }]
	});
}
//初始化表格
function initTable() {
	var $table = $('#table');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '用户ID',
                    	field: 'userId',
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        title: '用户头像',
                        field: 'avator',
                        align: 'center',
                        class:'headerImg',
                        valign: 'middle',
                        events: headerImgEvents,
                        formatter:headerImgFormatter
                    },{
                        title: '用户名称',
                        field: 'userName',
                        width:200,
                        align: 'center',
                        valign: 'middle'
                    }, {
                        title: '用户手机号码',
                        field:'phone',
                        width:150,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '用户邮箱',
                        field:'email',
                        width:200,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '用户类型',
                        field:'type',
                        width:100,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '用户性别',
                        field:'sex',
                        width:80,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '用户生日',
                        field:'birthday',
                        width:200,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//用户头像展示
function headerImgFormatter(value, row, index){
	var imageURL=$("input[name='imageURL']").val()+"/";
	return '<img src="'+imageURL+row.avator+'" width="100px" height="40px" style="" class="userHeader"></img>';
}
//头像点击事件
var headerImgEvents={
		'click .userHeader': function (e, value, row, index) {
			var repeatNum=1;
            //点击头像 放大图像
            $(".headerImg").click(function(){
            	if(repeatNum==0){
            		return;
            	}
            	repeatNum--;
        		top.window.layer.open({
        			 type: 2, 
        			 title :'用户头像',
        			 maxmin: true,
        			 content: ['../../common/image/show/'+row.avator,'yes'],
        			 area: ['800px', '600px']
        		});
        	});
        }
};
//操作按钮
function operateFormatter(value, row, index) {
	var buttons=[];
	var viewUserPermission="$shiro.hasPermission('system:user:view')";
	alert(viewUserPermission);
	if(viewUserPermission&&viewUserPermission=="true"){
		buttons.push('<button type="button" class="btn btn-default btn-sm userDetail"><span class="glyphicon glyphicon-eye-open"></span>查看</button>');
	}
	var resetPasswordPermission="$shiro.hasPermission('system:user:resetPassword')";
	if(resetPasswordPermission&&resetPasswordPermission=="true"){
		buttons.push('<button type="button" class="btn btn-default btn-sm resetPassword"><span class="glyphicon glyphicon-wrench"></span>重置密码</button>');
	}
	var editUserPermission="$shiro.hasPermission('system:user:edit')";
	if(editUserPermission&&editUserPermission=="true"){
		buttons.push('<button type="button" class="btn btn-default btn-sm editUser"><span class="glyphicon glyphicon-edit"></span>编辑</button>');
	}
	var deleteUserPermission="$shiro.hasPermission('system:user:delete')";
	if(deleteUserPermission&&deleteUserPermission=="true"){
		buttons.push('<button type="button" class="btn btn-default btn-sm deleteUser"><span class="glyphicon glyphicon-trash"></span>删除</button>');
	}
	var allowRolePermission="$shiro.hasPermission('system:user:allowRole')";
	if(allowRolePermission&&allowRolePermission=="true"){
		buttons.push('<button type="button" class="btn btn-default btn-sm allowRole"><span class="glyphicon glyphicon-tree-deciduous"></span>分配角色</button>');
	}
	return buttons.join('');
}
//表格点击事件
var operateEvents = {
        //用户详情
		'click .userDetail': function (e, value, row, index) {
			top.window.layer.open({
				 type: 2, 
				 title :'查看用户详情',
				 maxmin: true,
				 content: ['../../system/user/view/'+row.userId,'yes'],
				 area: ['800px', '600px']
			});
        },
        //重置用户密码
        'click .resetPassword': function (e, value, row, index) {
        	layer.confirm('你确定要重置这个用户的密码吗?(重置密码为123456)', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/user/resetPwd",{userId:row.userId,userName:row.userName,password:'123456',_method:'put'},function(data){
        			if(data.code==200){
        				top.layer.msg(data.msg,{
            				icon: 1,
            				skin: 'demo-class',
            				title :'信息',
            				time:500,
            			    area: ['300px', '160px'],
            			    offset: 'rb'
            			});
        				layer.close(index);
        			}else{
        				layer.alert(data.msg);
        			}
        		});
        	});
        },
        //编辑用户
        'click .editUser': function (e, value, row, index) {
        	top.window.layer.open({
				 type: 2, 
				 title :'编辑用户',
				 maxmin: true,
				 content: ['../../system/user/edit/'+row.userId,'yes'],
				 area: ['800px', '600px'],
				 end:function(){
					 queryUser();
				 }
			});
        },
        //删除用户
        'click .deleteUser': function (e, value, row, index) {
        	layer.confirm('你确定要删除用户【'+row.userName+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/user/delete/"+row.userId,{_method:'delete'},function(data){
        			if(data.code==200){
        				top.layer.msg(data.msg,{
            				icon: 1,
            				skin: 'demo-class',
            				title :'信息',
            				time:500,
            			    area: ['300px', '160px'],
            			    offset: 'rb',
            			    end :function(){
            			    	queryUser();
            			    }
            			});
        			}else{
        				layer.alert(data.msg);
        			}
        		});
        	});
        },
        //用户分配角色
        'click .allowRole': function (e, value, row, index) {
        	top.window.layer.open({
				 type: 2, 
				 title :'用户分配角色',
				 maxmin: true,
				 content: ['../../system/user/allowRole?userId='+row.userId,'yes'],
				 area: ['600px', '400px']
			});
        }
};
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	userName:$("input[name='userName']").val(),
    	userPhone:$("input[name='userPhone']").val(),
    	userEmail:$("input[name='userEmail']").val()
    };
}
//表单查询
function queryUser(){
	var $table = $('#table');    
	$table.bootstrapTable("refresh");
}
$(function(){
	initTable();
	msg('用户信息加载完毕');
});