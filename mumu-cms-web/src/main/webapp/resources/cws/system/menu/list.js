/**
 * 菜单列表
 */
layui.use(['laypage', 'layer','element']);
//添加用户
function addMenu(){
	top.window.layer.open({
		 type: 2, 
		 title :'添加菜单',
		 maxmin: true,
		 content: ['../../system/menu/add','yes'],
		 area: ['900px', '700px'],
		 end:function(){
			 queryMenu();
		 }
	});
}
//初始化表格
function initTable() {
	var $table = $('#menuTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '菜单ID',
                    	field: 'menuId',
                        width:40,
                        align: 'center',
                        valign: 'middle',
                    }, {
                    	title: '父菜单ID',
                    	field:'parentMenuId',
                        align: 'center',
                        valign: 'middle',
                    },{
                        title: '菜单内码',
                        field:'menuCode',
                        width:50,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单名称',
                        field: 'menuName',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单地址',
                        field:'menuUrl',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单编号',
                        field:'menuNum',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单图标',
                        field:'menuIcon',
                        align: 'center',
                        valign: 'middle',
                        formatter:function(value, row, index){
                        	return "<span class='"+row.menuIcon+"'></span>";
                        }
                    },{
                        title: '可见性',
                        field:'menuVisible',
                        width:80,
                        align: 'center',
                        valign: 'middle',
                        formatter:function(value, row, index){
                        	/*if(value=='show'){
                        		return "<span class=''></span>";
                        	}else{
                        		return "<span class=''></span>";
                        	}*/
                        	return value;
                        }
                    },{
                        title: '父菜单名称',
                        field:'parentMenuName',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
/*function operateFormatter(value, row, index) {
	var buttons=[];
	buttons.push('<button type="button" class="btn btn-default btn-sm menuDetail"><span class="glyphicon glyphicon-eye-open"></span>查看</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm editMenu"><span class="glyphicon glyphicon-edit"></span>编辑</button>');
	buttons.push('<button type="button" class="btn btn-default btn-sm deleteMenu"><span class="glyphicon glyphicon-trash"></span>删除</button>');
	if(row.parentMenuId!=0){
		buttons.push('<button type="button" class="btn btn-default btn-sm menuPermission"><span class="glyphicon glyphicon-leaf"></span>权限</button>');
	}else{
		buttons.push('<button type="button" class="btn btn-default btn-sm subMenu"><span class="glyphicon glyphicon-th-large"></span>菜单</button>');
	}
    return buttons.join('');
}*/
//表格点击事件
var operateEvents = {
        //用户详情
		'click .menuDetail': function (e, value, row, index) {
			top.window.layer.open({
				 type: 2, 
				 title :'查看菜单详情',
				 maxmin: true,
				 content: ['../../system/menu/view?menuId='+row.menuId,'yes'],
				 area: ['800px', '600px']
			});
        },
        //编辑用户
        'click .editMenu': function (e, value, row, index) {
        	top.window.layer.open({
				 type: 2, 
				 title :'编辑菜单',
				 maxmin: true,
				 content: ['../../system/menu/edit?menuId='+row.menuId,'yes'],
				 area: ['900px', '700px'],
				 end:function(){
					 queryMenu();
				 }
			});
        },
        //删除用户
        'click .deleteMenu': function (e, value, row, index) {
        	layer.confirm('你确定要删除菜单【'+row.menuName+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/menu/delete",{menuId:row.menuId,_method:'delete'},function(data){
        			top.layer.msg(data.msg,{
    					icon: 1,
    					skin: 'demo-class',
    					title :'信息',
    					time:500,
    				    area: ['300px', '160px'],
    				    offset: 'rb',
    				    end :function(){
    				    	if(data.code==200){
    				    		layer.close(index);   
    				    		//刷新表格数据
    				    		queryMenu();
    						}
    				    }
    				});
        		});
        	});
        },
        //子菜单
        'click .subMenu': function (e, value, row, index) {
        	top.window.layer.open({
				 type: 2, 
				 title :'子菜单',
				 maxmin: true,
				 content: ['../../system/menu/subMenu?parentMenuId='+row.menuId,'yes'],
				 area: ['800px', '600px']
			});
        },
        //菜单权限
        'click .menuPermission': function (e, value, row, index) {
        	top.window.layer.open({
        		type: 2, 
        		title :'菜单权限',
        		maxmin: true,
        		content: ['../../system/menu/menuPermission?menuId='+row.menuId,'yes'],
        		area: ['800px', '600px']
        	});
        }
};
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	parentMenuId:$('#parentMenuSelect').children('option:selected').val(),
    	menuName:$("input[name='menuName']").val()
    };
}
//表单查询
function queryMenu(){
	var $table = $('#menuTable');    
	$table.bootstrapTable("refresh");
}
$(function(){
	initTable();
	msg('菜单信息加载完毕');
});