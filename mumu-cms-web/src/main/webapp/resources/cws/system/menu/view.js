/**
 * 添加菜单
 */
$(function(){
	layui.use(['form', 'layedit', 'laydate'], function(){
		var form = layui.form(),layer = layui.layer,layedit = layui.layedit,laydate = layui.laydate;
		//角色描述编辑器
		var menuDescIndex=layedit.build('menuDesc',{
			tool: [
				'strong' //加粗
				,'italic' //斜体
				,'underline' //下划线
				,'|' //分割线
				,'left' //左对齐
				,'center' //居中对齐
				,'right' //右对齐
			]
		}); //建立编辑器
	});
});