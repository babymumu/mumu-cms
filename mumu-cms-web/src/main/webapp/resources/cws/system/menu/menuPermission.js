/**
 * 菜单权限
 */
//添加权限
function addMenuPermission(menuId){
	var $table = $('#menuPermissionTable');
	$table.bootstrapTable("insertRow",{
		index:0,
		row:{
			state:false,
			permissionId:0,
			permissionCode:'',
			permissionName:'',
			permission:'',
			menuId:menuId,
			operate:''
		}
	});
}
//初始化表格
function initTable() {
	var $table = $('#menuPermissionTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '权限ID',
                    	field: 'permissionId',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    }, {
                        title: '权限内码',
                        field:'permissionCode',
                        align: 'center',
                        valign: 'middle',
                        editable:true
                    },{
                        title: '权限名称',
                        field: 'permissionName',
                        align: 'center',
                        valign: 'middle',
                        editable:true
                    },{
                        title: '权限值',
                        field:'permission',
                        align: 'center',
                        valign: 'middle',
                        editable:true
                    },{
                        title: '菜单ID',
                        field:'menuId',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
function operateFormatter(value, row, index) {
    return [
        '<button type="button" class="btn btn-default btn-sm deleteMenuPermission"><span class="glyphicon glyphicon-trash"></span>删除</button>',
        '<button type="button" class="btn btn-default btn-sm saveMenuPermission"><span class="glyphicon glyphicon-ok"></span>保存</button>'
    ].join('');
}
//表格点击事件
var operateEvents = {
        //删除菜单权限
        'click .deleteMenuPermission': function (e, value, row, index) {
        	layer.confirm('你确定要菜单权限【'+row.permission+'】吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/menu/deleteMenuPermission",{permissionId:row.permissionId,_method:'delete'},function(data){
        			if(data.code==200){
        				var $table = $('#menuPermissionTable');    
        				$table.bootstrapTable("refresh");
            			layer.close(index); 
        			}else{
        				layer.alert(data.msg);
        			}
        		});
        	});
        },
		//保存菜单权限
		'click .saveMenuPermission': function (e, value, row, index) {
			$.post('../../system/menu/saveMenuPermission',row,function(data){
				if(data.code==200){
					var $table = $('#menuPermissionTable');    
					$table.bootstrapTable("refresh");
				}else{
					layer.alert(data.msg);
				}
			});
		}
};
$(function(){
	initTable();
});