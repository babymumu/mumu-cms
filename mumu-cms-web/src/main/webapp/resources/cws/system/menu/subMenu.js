/**
 * 菜单列表
 */
//初始化表格
function initTable() {
	var $table = $('#subMenuTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '菜单ID',
                    	field: 'menuId',
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    }, {
                        title: '菜单内码',
                        field:'menuCode',
                        width:150,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        title: '菜单名称',
                        field: 'menuName',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单地址',
                        field:'menuUrl',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单编号',
                        field:'menuNum',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '菜单图标',
                        field:'menuIcon',
                        align: 'center',
                        valign: 'middle',
                        formatter:function(value, row, index){
                        	return "<span class='"+row.menuIcon+"'></span>";
                        }
                    },{
                        title: '可见性',
                        field:'menuVisible',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '父菜单名称',
                        field:'parentMenuId',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    }]
        });
}
$(function(){
	initTable();
});