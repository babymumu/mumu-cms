/**
 * 添加菜单
 */
$(function(){
	layui.use(['form', 'layedit', 'laydate'], function(){
		var form = layui.form(),layer = layui.layer,layedit = layui.layedit,laydate = layui.laydate;
		//角色描述编辑器
		var menuDescIndex=layedit.build('menuDesc',{
			tool: [
				'strong' //加粗
				,'italic' //斜体
				,'underline' //下划线
				,'|' //分割线
				,'left' //左对齐
				,'center' //居中对齐
				,'right' //右对齐
			]
		}); //建立编辑器
		//自定义验证规则
		form.verify({
			realName:function(roleName){
			    if(roleName==null||roleName==''){
			       return "角色名称不能为空";
			    }
			    if(roleName=="123"){
			    	return "角色名称已存在";
			    }
			}
		});
		
		//监听提交
		form.on('submit(addMenuForm)', function(data){
			//校验
		    $.get("../../system/menu/exists?menuCode="+data.field.menuCode+"&menuName="+data.field.menuName,function(checkData){
		    	if(checkData.code==500){
		    		layer.alert(checkData.msg);
		    		return false;
		    	}else if(checkData.code==200){
		    		var remark=layedit.getContent(menuDescIndex);
		    		data.field.remark=remark;
					$.post("../../system/menu/add",data.field,function(data){
						//保存成功 关闭当前页
						top.layer.msg(data.msg,{
							icon: 1,
							skin: 'demo-class',
							title :'信息',
							time:500,
						    area: ['300px', '160px'],
						    offset: 'rb',
						    end :function(){
						    	if(data.code==200){
						    		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
									parent.layer.close(index); //再执行关闭   
								}
						    }
						});
					});
		    	}
		    });
		    return false;
		});
		
		//菜单图标选择
		$("input[name='menuIcon']").click(function(e){
			layer.open({
				 type: 2, 
				 title :'选择图标',
				 maxmin: true,
				 content: ['../../common/image/icon','yes'],
				 area: ['800px', '600px']
			});
		});
	});
});