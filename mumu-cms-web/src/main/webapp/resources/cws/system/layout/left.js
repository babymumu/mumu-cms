/**
 * 左侧菜单栏
 */
function menuClick(text,src){
	//处理location
	handleLocation(src);

	var maintabs=parent.window.$('#maintabs');
	var exists=maintabs.tabs('exists',text);
	if(!exists){
		var iframe='<iframe scrolling="yes" frameborder="0" src="../../'+src+'" style="width: 100%; height: 99.5%;"></iframe>';
		if(src==null||src==''){
			iframe=null;
		}
		maintabs.tabs('add',{
			title: text,
			selected: true,
			closable:true,
			content:iframe
		});
	}else{
		maintabs.tabs('select', text);
	}
}
function menuDbClick(text,src) {
	handleLocation(src);
	var maintabs=parent.window.$('#maintabs');
	maintabs.tabs('close',text);
	var iframe='<iframe scrolling="yes" frameborder="0" src="../../'+src+'" style="width: 100%; height: 99.5%;"></iframe>';
	if(src==null||src==''){
		iframe=null;
	}
	maintabs.tabs('add',{
		title: text,
		selected: true,
		closable:true,
		content:iframe
	});
}
//处理location
function handleLocation(src){
	if(src==null||src==''){
		return;
	}
	var locationURL=top.window.location.href;
	var locations=locationURL.split("#");
	if(locations.length>=2){
		locationURL=locations[0];
	}
	top.window.location.href=locationURL+"#"+src;
}

$(function(){
	$(".container-fluid .sidebar-menu ul li").click(function(){
		$(".container-fluid .sidebar-menu ul li").removeClass("menu-second-selected");
		$(this).addClass("menu-second-selected");
	});

	/*$(".container-fluid .sidebar-menu .menu-second").mousedown(function(e) {
		if (3 == e.which) {
			document.oncontextmenu = function() {return false;}
			$("#contextMenu").hide();
			$("#contextMenu").attr("style","display: block; position: fixed; top:"
				+ e.pageY
				+ "px; left:"
				+ e.pageX
				+ "px; width: 280px;");
			$("#contextMenu").show();
		}
	});*/
});
