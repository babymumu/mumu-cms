/**
 * 菜单列表
 */
//日志统计
function logStatistics(){
	top.layer.tab({
		 area: ['900px', '600px'],
		 maxmin: true,
		 shift:3,
		 tab: [{
		   title: '日志统计', 
		   content: '<iframe scrolling="yes" frameborder="0" src="../../system/log/logStatistics" style="width: 100%; height: 99.5%;"></iframe>'
		 }]
	});
}
//初始化表格
function initTable() {
	var $table = $('#logTable');    
	$table.bootstrapTable({
           columns: [{
                        field: 'state',
                        checkbox: true,
                        width:40,
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                        field: 'userLogId',
                        align: 'center',
                        valign: 'middle',
                        visible:false
                    },{
                    	title: '用户名称',
                    	field: 'userName',
                        width:40,
                        align: 'center',
                        valign: 'middle'
                    },{
                    	title: '访问IP',
                    	field: 'ip',
                        width:40,
                        align: 'center',
                        valign: 'middle'
                    },{
                    	title: '访问方法',
                    	field: 'method',
                        align: 'center',
                        valign: 'middle'
                    },  {
                        title: '内容',
                        field:'content',
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '花费时间',
                        field: 'usetime',
                        width:100,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '日志时间',
                        field: 'createTime',
                        width:200,
                        align: 'center',
                        valign: 'middle'
                    },{
                        title: '操作',
                        field: 'operate',
                        align: 'center',
                        valign: 'middle',
                        events: operateEvents,
                        formatter: operateFormatter
                    }]
        });
}
//操作按钮
function operateFormatter(value, row, index) {
   /* return [
        '<button type="button" class="btn btn-default btn-sm deleteUser"><span class="glyphicon glyphicon-trash"></span>删除</button>'
    ].join('');*/
   return logPermissionButtons.join('');
}
//表格点击事件
var operateEvents = {
        //删除用户
        'click .deleteUser': function (e, value, row, index) {
        	layer.confirm('你确定要删除该日志吗?', {icon: 3, title:'提示'}, function(index){
        		$.post("../../system/log/delete",{userLogId:row.userLogId,_method:'delete'},function(data){
        			top.layer.msg(data.msg,{
    					icon: 1,
    					skin: 'demo-class',
    					title :'信息',
    					time:500,
    				    area: ['300px', '160px'],
    				    offset: 'rb',
    				    end :function(){
    				    	if(data.code==200){
    				    		layer.close(index);   
    				    		//刷新表格数据
    				    		queryLog();
    						}
    				    }
    				});
        		});
        	});
        }
};
function queryParams(params) {
    return {
    	beginIndex:params.offset,
    	pageSize:params.limit,
    	startDate:$("input[name='startDate']").val(),
    	endDate:$("input[name='endDate']").val()
    };
}
//表单查询
function queryLog(){
	var $table = $('#logTable');    
	$table.bootstrapTable("refresh");
}
$(function(){
	initTable();
	msg('日志信息加载完毕');
	
	$('.datepicker').datepicker({
	      viewMode: 'years',
	      language:  'zh',
	      autoclose: 1,
	      language:  'zh-CN',
	      format: 'yyyy-mm-dd',  
	      todayHighlight: 1 
	    });
});