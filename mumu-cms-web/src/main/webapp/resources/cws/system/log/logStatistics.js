/**
 * 日志统计
 */
$(function () {
    //$.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {
      $.get('../../system/log/logStatisticsData',function(data){	
    	console.log(data);
        $('#logContainer').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: '用户日志统计图'
            },
            xAxis: {
                type: 'datetime',
                labels: {  
                    step: 1,   
                    formatter: function () {  
                        return Highcharts.dateFormat('%m-%d', this.value);  
                    }  
                } 
            },
            yAxis: {
                title: {
                    text: '日志数量(W)'
                }
            },
            tooltip: {  
                formatter: function () {  
                    return '<b>' + this.series.name + '</b><br/>' +  
                    Highcharts.dateFormat('%Y-%m-%d', this.x) + ':数量' + this.y.toFixed(0);  
                }  
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
            series: [{
                type: 'area',
                name: '操作次数',
                data: data
            }]
        });
    });
});