﻿var menuData=[{id:1,text:'业务申请',src:'',children:[{id:101,text:'请假申请',src:''},{id:102,text:'出差申请',src:''},{id:13,text:'借款申请',src:''},
                                                 {id:13,text:'并行会签申请',src:''},{id:13,text:'分支监听申请',src:''},{id:13,text:'商机申请',src:''}
                                                 ,{id:13,text:'提交作业申请',src:''},{id:13,text:'word请假单申请',src:''},{id:13,text:'工程客户申请',src:''}
                                                 ,{id:13,text:'员工借款申请 岗位',src:''}]},
			  {id:2,text:'权限设置',src:'',children:[{id:21,text:'用户管理 ',src:'./list.html'},{id:22,text:'角色管理',src:''},{id:23,text:'菜单管理',src:''},{id:24,text:'权限管理',src:''}]},
			  {id:3,text:'系统设置',src:'',children:[{id:31,text:'数据字典',src:''},{id:32,text:'日志管理',src:''},{id:33,text:'导出设置',src:''}]}];
			  
$(document).ready(function(){
	var menuBar=$("#menuBar");
	for(var i=0,len=menuData.length;i<len;i++){
		var managerBar=menuData[i];
		var managerPanel=menuBar.accordion('add', {
			title: managerBar.text,
			selected: false,
			bodyCls:'bodyStyle',
			border:false,
			content:"<ul class='' id="+managerBar.id+" class='easyui-tree' data-options=''></ul>"
		});
		$("#"+managerBar.id).tree({
			data:managerBar.children,
			onClick:function(node){
				treeClick(node);
			}
		});
	}
	
	//树点击事件
	function treeClick(node){
		var maintabs=parent.window.$('#maintabs');
		var exists=maintabs.tabs('exists',node.text);
		if(!exists){
			maintabs.tabs('add',{
				id:node.id,
				title: node.text,
				selected: true,
				closable:true
			});
		}
	}
});