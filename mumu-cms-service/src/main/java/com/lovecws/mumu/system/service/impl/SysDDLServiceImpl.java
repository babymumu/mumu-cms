package com.lovecws.mumu.system.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.system.dao.SysDDLDao;
import com.lovecws.mumu.system.entity.SysDDL;
import com.lovecws.mumu.system.service.SysDDLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=true)
public class SysDDLServiceImpl implements SysDDLService {

	@Autowired
	private SysDDLDao ddlDao;

	@Override
	public PageBean<SysDDL> listPage(String ddlCode, String ddlName, int pageNum, int pageSize) {
		PageParam pageParam=new PageParam(pageNum, pageSize);
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("ddlCode", ddlCode);
		paramMap.put("ddlName", ddlName);
		paramMap.put("ddlStatus", PublicEnum.NORMAL.value());
		return ddlDao.listPage(pageParam, paramMap);
	}

	@Override
	@Transactional(readOnly=false)
	public SysDDL addSysDDL(SysDDL ddl) {
		ddl.setCreateTime(new Date());
		ddl.setDdlStatus(String.valueOf(PublicEnum.NORMAL.value()));
		return ddlDao.insert(ddl);
	}

	@Override
	public List<SysDDL> getSysDDLByCondition(String ddlCode, String ddlName) {
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("ddlCode", ddlCode);
		paramMap.put("ddlName", ddlName);
		paramMap.put("ddlStatus", PublicEnum.NORMAL.value());
		return ddlDao.listByColumn(paramMap);
	}

	@Override
	@Transactional(readOnly=false)
	public void deleteSysDDLByDDLCode(String ddlCode) {
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("ddlCode", ddlCode);
		ddlDao.delete(paramMap);
	}
	
	@Override
	@Transactional(readOnly=false)
	public SysDDL saveOrUpdateSysDDL(SysDDL ddl) {
		if(ddl.getDdlId()==0){
			//添加数据字典
			addSysDDL(ddl);
		}else{
			//更新数据字典
			ddlDao.update(ddl);
		}
		return null;
	}
	
	@Override
	@Transactional(readOnly=false)
	public void deleteSysDDLById(String ddlId) {
		ddlDao.delete(ddlId);
	}
	
	@Override
	public SysDDL getSysDDLById(String ddlId) {
		return ddlDao.getById(ddlId);
	}
	
	@Override
	@Transactional(readOnly=false)
	public void updateSysDDLByDDLCode(String ddlCode,String ddlName,String remark) {
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("ddlCode", ddlCode);
		paramMap.put("ddlName", ddlName);
		paramMap.put("remark", remark);
		ddlDao.update(paramMap);
	}
}
