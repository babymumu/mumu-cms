package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialProductDao;
import com.lovecws.mumu.official.entity.OfficialProductEntity;
import com.lovecws.mumu.official.service.OfficialProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialProductServiceImpl implements OfficialProductService {

    @Autowired
    private OfficialProductDao productDao;

    @Override
    public PageBean<OfficialProductEntity> listPage(String searchContent, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("title", searchContent);
        paramMap.put("categoryId", searchContent);
        //paramMap.put("tatus", PublicEnum.NORMAL.value());
        return productDao.listPage(pageParam,paramMap);
    }

    @Override
    @Transactional(readOnly=false)
    public void addProduct(OfficialProductEntity product) {
        productDao.insert(product);
    }

    @Override
    public OfficialProductEntity getProductById(int productId) {
        return productDao.getById(String.valueOf(productId));
    }

    @Override
    @Transactional(readOnly=false)
    public void updateProduct(OfficialProductEntity product) {
        productDao.update(product);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteProductById(int productId) {
        productDao.delete(String.valueOf(productId));
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteProductByCategoryId(int categoryId) {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryId", categoryId);
        productDao.delete(paramMap);
    }
}
