package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialFeedbackDao;
import com.lovecws.mumu.official.entity.OfficialFeedbackEntity;
import com.lovecws.mumu.official.service.OfficialFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialFeedbackServiceImpl implements OfficialFeedbackService {

    @Autowired
    private OfficialFeedbackDao feedbackDao;

    @Override
    public PageBean<OfficialFeedbackEntity> listPage(String searchContent, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("searchContent", searchContent);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return feedbackDao.listPage(pageParam, paramMap);
    }

    @Override
    public OfficialFeedbackEntity getFeedbackIdById(int feedbackId) {
        return feedbackDao.getById(String.valueOf(feedbackId));
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteFeedbackById(int feedbackId) {
        feedbackDao.delete(String.valueOf(feedbackId));
    }

    @Override
    @Transactional(readOnly=false)
    public void addFeedback(OfficialFeedbackEntity feedback) {
        feedbackDao.insert(feedback);
    }
}
