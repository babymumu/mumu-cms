package com.lovecws.mumu.official.service.impl;

/**
 * Created by Administrator on 2017/3/14.
 */

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialMessageDao;
import com.lovecws.mumu.official.entity.OfficialMessageEntity;
import com.lovecws.mumu.official.service.OfficialMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialMessageServiceImpl implements OfficialMessageService{

    @Autowired
    private OfficialMessageDao messageDao;

    @Override
    public PageBean<OfficialMessageEntity> listPage(String searchContent, int pageNum, int pageSize) {
       PageParam pageParam=new PageParam(pageNum, pageSize);
       Map<String,Object> paramMap=new HashMap<String,Object>();
       paramMap.put("searchContent", searchContent);
       paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
       return messageDao.listPage(pageParam, paramMap);
    }

    @Override
    public OfficialMessageEntity getMessageIdById(int messageId) {
        return messageDao.getById(String.valueOf(messageId));
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteMessageById(int messageId) {
        messageDao.delete(String.valueOf(messageId));
    }

    @Override
    @Transactional(readOnly=false)
    public void addMessage(OfficialMessageEntity message) {
        messageDao.insert(message);
    }
}
