package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialNewsCategoryDao;
import com.lovecws.mumu.official.entity.OfficialNewsCategoryEntity;
import com.lovecws.mumu.official.service.OfficialNewsCategoryService;
import com.lovecws.mumu.official.service.OfficialNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialNewsCategoryServiceImpl implements OfficialNewsCategoryService {

    @Autowired
    private OfficialNewsCategoryDao newsCategoryDao;
    @Autowired
    private OfficialNewsService newsService;

    @Override
    public PageBean<OfficialNewsCategoryEntity> listPage(String categoryName, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return newsCategoryDao.listPage(pageParam, paramMap);
    }

    @Override
    @Transactional(readOnly=false)
    public void addNewsCategory(OfficialNewsCategoryEntity newsCategory) {
        newsCategoryDao.insert(newsCategory);
    }

    @Override
    public OfficialNewsCategoryEntity getNewsCategoryById(int categoryId) {
        return newsCategoryDao.getById(String.valueOf(categoryId));
    }

    @Override
    @Transactional(readOnly=false)
    public void updateNewsCategory(OfficialNewsCategoryEntity newsCategory) {
        newsCategoryDao.update(newsCategory);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteNewsCategoryById(int categoryId) {
        newsService.deleteNewsByCategoryId(categoryId);
        newsCategoryDao.delete(String.valueOf(categoryId));
    }

    @Override
    public List<OfficialNewsCategoryEntity> getNewsCategoryByCondition(String categoryName) {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return newsCategoryDao.listByColumn(paramMap);
    }
}
