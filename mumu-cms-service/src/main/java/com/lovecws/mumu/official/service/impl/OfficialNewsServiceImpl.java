package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialNewsDao;
import com.lovecws.mumu.official.entity.OfficialNewsEntity;
import com.lovecws.mumu.official.service.OfficialNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialNewsServiceImpl implements OfficialNewsService {

    @Autowired
    private OfficialNewsDao newsDao;

    @Override
    public PageBean<OfficialNewsEntity> listPage(String categoryName, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return newsDao.listPage(pageParam,paramMap);
    }

    @Override
    @Transactional(readOnly=false)
    public void addNews(OfficialNewsEntity news) {
        newsDao.insert(news);
    }

    @Override
    public OfficialNewsEntity getNewsById(int newsId) {
        return newsDao.getById(String.valueOf(newsId));
    }

    @Override
    @Transactional(readOnly=false)
    public void updateNews(OfficialNewsEntity news) {
        newsDao.update(news);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteNewsById(int newsId) {
        newsDao.delete(String.valueOf(newsId));
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteNewsByCategoryId(int categoryId) {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryId", categoryId);
        newsDao.delete(paramMap);
    }
}
