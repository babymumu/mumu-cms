package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialServiceCategoryDao;
import com.lovecws.mumu.official.entity.OfficialServiceCategoryEntity;
import com.lovecws.mumu.official.service.OfficialServiceCategoryService;
import com.lovecws.mumu.official.service.OfficialServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialServiceCategoryServiceImpl implements OfficialServiceCategoryService {

    @Autowired
    private OfficialServiceCategoryDao serviceCategoryDao;
    @Autowired
    private OfficialServiceService serviceService;

    @Override
    public PageBean<OfficialServiceCategoryEntity> listPage(String categoryName, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return serviceCategoryDao.listPage(pageParam,paramMap);
    }

    @Override
    @Transactional(readOnly=false)
    public void addServiceCategory(OfficialServiceCategoryEntity serviceCategory) {
        serviceCategoryDao.insert(serviceCategory);
    }

    @Override
    public OfficialServiceCategoryEntity getServiceCategoryById(int categoryId) {
        return serviceCategoryDao.getById(String.valueOf(categoryId));
    }

    @Override
    @Transactional(readOnly=false)
    public void updateServiceCategory(OfficialServiceCategoryEntity serviceCategory) {
        serviceCategoryDao.update(serviceCategory);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteServiceCategoryById(int categoryId) {
        //删除服务分类下的服务
        serviceService.deleteServiceByCategoryId(categoryId);

        //删除服务分类
        serviceCategoryDao.delete(String.valueOf(categoryId));
    }

    @Override
    public List<OfficialServiceCategoryEntity> getServiceCategoryByCondition(String categoryName) {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return serviceCategoryDao.listByColumn(paramMap);
    }
}
