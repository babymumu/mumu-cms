package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialServiceDao;
import com.lovecws.mumu.official.entity.OfficialServiceEntity;
import com.lovecws.mumu.official.service.OfficialServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialServiceServiceImpl implements OfficialServiceService {

    @Autowired
    private OfficialServiceDao serviceDao;

    @Override
    public PageBean<OfficialServiceEntity> listPage(String categoryName, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return serviceDao.listPage(pageParam,paramMap);
    }

    @Override
    @Transactional(readOnly=false)
    public void addService(OfficialServiceEntity service) {
        serviceDao.insert(service);
    }

    @Override
    public OfficialServiceEntity getServiceById(int serviceId) {
        return serviceDao.getById(String.valueOf(serviceId));
    }

    @Override
    @Transactional(readOnly=false)
    public void updateService(OfficialServiceEntity service) {
        serviceDao.update(service);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteServiceById(int serviceId) {
        serviceDao.delete(String.valueOf(serviceId));
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteServiceByCategoryId(int categoryId) {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryId", categoryId);
        serviceDao.delete(paramMap);
    }
}
