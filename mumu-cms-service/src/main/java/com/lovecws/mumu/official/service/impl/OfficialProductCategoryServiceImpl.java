package com.lovecws.mumu.official.service.impl;

import com.lovecws.mumu.common.core.enums.PublicEnum;
import com.lovecws.mumu.common.core.page.PageBean;
import com.lovecws.mumu.common.core.page.PageParam;
import com.lovecws.mumu.official.dao.OfficialProductCategoryDao;
import com.lovecws.mumu.official.entity.OfficialProductCategoryEntity;
import com.lovecws.mumu.official.service.OfficialProductCategoryService;
import com.lovecws.mumu.official.service.OfficialProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/14.
 */
@Service
@Transactional(isolation= Isolation.DEFAULT,propagation= Propagation.REQUIRED,readOnly=true)
public class OfficialProductCategoryServiceImpl implements OfficialProductCategoryService {

    @Autowired
    private OfficialProductCategoryDao productCategoryDao;
    @Autowired
    private OfficialProductService productService;

    @Override
    public PageBean<OfficialProductCategoryEntity> listPage(String categoryName, int pageNum, int pageSize) {
        PageParam pageParam=new PageParam(pageNum, pageSize);
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return productCategoryDao.listPage(pageParam, paramMap);
    }

    @Override
    @Transactional(readOnly=false)
    public void addProductCategory(OfficialProductCategoryEntity productCategoryEntity) {
        productCategoryDao.insert(productCategoryEntity);
    }

    @Override
    public OfficialProductCategoryEntity getProductCategoryById(int categoryId) {
        return productCategoryDao.getById(String.valueOf(categoryId));
    }

    @Override
    @Transactional(readOnly=false)
    public void updateProductCategory(OfficialProductCategoryEntity productCategory) {
        productCategoryDao.update(productCategory);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteProductCategoryById(int categoryId) {
        //删除该分类下的产品
        productService.deleteProductByCategoryId(categoryId);

        //删除该产品分类
        productCategoryDao.delete(String.valueOf(categoryId));
    }

    @Override
    public List<OfficialProductCategoryEntity> getProductCategoryByCondition(String categoryName) {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("categoryName", categoryName);
        paramMap.put("categoryStatus", PublicEnum.NORMAL.value());
        return productCategoryDao.listByColumn(paramMap);
    }
}
