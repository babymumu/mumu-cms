package com.lovecws.mumu.velocity.tag;

import com.lovecws.mumu.common.core.utils.PropertiesLoader;
import org.apache.velocity.tools.Scope;
import org.apache.velocity.tools.config.DefaultKey;
import org.apache.velocity.tools.config.ValidScope;

/**
 * 获取路径tag
 * @author lgan
 */
@DefaultKey("url")
@ValidScope(Scope.APPLICATION)
public class URLTag {

	private static String ZIMG="";
	static{
		PropertiesLoader loader=new PropertiesLoader("classpath:properties/zimg.properties");
		ZIMG=loader.getProperty("ZIMG_URL");
	}
	
	/**
	 * 获取zimg服务器下载路径
	 * @return
	 */
	public String zimg(){
		return ZIMG;
	}
}
