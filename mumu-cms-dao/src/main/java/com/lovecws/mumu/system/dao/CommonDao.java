package com.lovecws.mumu.system.dao;

import com.lovecws.mumu.system.entity.DBField;
import com.lovecws.mumu.system.entity.DBTable;

import java.util.List;

public interface CommonDao{

	public List<DBTable> getAllTable();

	public List<DBField> getAllField(String tableName);

	public List<List<Object>> getAllData(String tableName, String fields, String params);

}
