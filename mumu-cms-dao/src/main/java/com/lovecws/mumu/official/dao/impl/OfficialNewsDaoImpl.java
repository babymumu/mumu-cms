package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialNewsDao;
import com.lovecws.mumu.official.entity.OfficialNewsEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialNewsDaoImpl extends BaseDaoImpl<OfficialNewsEntity> implements OfficialNewsDao {

}
