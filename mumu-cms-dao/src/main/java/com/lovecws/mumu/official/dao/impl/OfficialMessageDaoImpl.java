package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialMessageDao;
import com.lovecws.mumu.official.entity.OfficialMessageEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialMessageDaoImpl extends BaseDaoImpl<OfficialMessageEntity> implements OfficialMessageDao {

}
