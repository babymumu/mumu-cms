package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialProductDao;
import com.lovecws.mumu.official.entity.OfficialProductEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialProductDaoImpl extends BaseDaoImpl<OfficialProductEntity> implements OfficialProductDao {

}
