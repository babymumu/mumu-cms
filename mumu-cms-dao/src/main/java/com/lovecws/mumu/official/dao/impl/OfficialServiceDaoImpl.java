package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialServiceDao;
import com.lovecws.mumu.official.entity.OfficialServiceEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialServiceDaoImpl extends BaseDaoImpl<OfficialServiceEntity> implements OfficialServiceDao {

}
