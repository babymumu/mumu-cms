package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialServiceCategoryDao;
import com.lovecws.mumu.official.entity.OfficialServiceCategoryEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialServiceCategoryDaoImpl extends BaseDaoImpl<OfficialServiceCategoryEntity> implements OfficialServiceCategoryDao {

}
