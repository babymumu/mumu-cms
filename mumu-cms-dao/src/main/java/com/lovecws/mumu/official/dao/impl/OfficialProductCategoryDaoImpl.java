package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialProductCategoryDao;
import com.lovecws.mumu.official.entity.OfficialProductCategoryEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialProductCategoryDaoImpl extends BaseDaoImpl<OfficialProductCategoryEntity> implements OfficialProductCategoryDao {

}
