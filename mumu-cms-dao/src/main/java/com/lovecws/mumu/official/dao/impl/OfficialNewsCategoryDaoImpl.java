package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialNewsCategoryDao;
import com.lovecws.mumu.official.entity.OfficialNewsCategoryEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialNewsCategoryDaoImpl extends BaseDaoImpl<OfficialNewsCategoryEntity> implements OfficialNewsCategoryDao {

}
