package com.lovecws.mumu.official.dao;

import com.lovecws.mumu.common.core.dao.BaseDao;
import com.lovecws.mumu.official.entity.OfficialFeedbackEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialFeedbackDao extends BaseDao<OfficialFeedbackEntity>{
}
