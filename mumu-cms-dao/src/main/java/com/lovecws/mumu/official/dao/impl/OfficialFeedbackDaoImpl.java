package com.lovecws.mumu.official.dao.impl;

import com.lovecws.mumu.common.core.dao.impl.BaseDaoImpl;
import com.lovecws.mumu.official.dao.OfficialFeedbackDao;
import com.lovecws.mumu.official.entity.OfficialFeedbackEntity;
import org.springframework.stereotype.Repository;

@Repository
public class OfficialFeedbackDaoImpl extends BaseDaoImpl<OfficialFeedbackEntity> implements OfficialFeedbackDao {

}
