package com.lovecws.mumu.official.dao;

import com.lovecws.mumu.common.core.dao.BaseDao;
import com.lovecws.mumu.official.entity.OfficialNewsEntity;

/**
 * Created by Administrator on 2017/3/14.
 */
public interface OfficialNewsDao extends BaseDao<OfficialNewsEntity>{
}
